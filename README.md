# Robot Framework library for JMS testing.

[![Maven Central](https://img.shields.io/maven-central/v/net.relaysoft.robotframework/jms-library.svg?style=flat-square)](http://search.maven.org/#artifactdetails%7Cnet.relaysoft.robotframework%7Cjms-library%7C1.0.0%7Cjar)

## Overview

Java Robot Framework for MQ based application testing. Provides basic JMS operations as keywords and supports JMS API version 1.1 and 2.x implemented libraries. Tested against _ActiveMQ_, _ActiveMQ Artemis_, and _IBM MQ_. 

This library can be used either with Maven Robot Framework plug-in, Robot Framework standalone JAR distribution, or as remote library with [jrobotremoteserver](https://github.com/ombre42/jrobotremoteserver).  

### Prerequisites

To execute within robot framework as standalone Java application.

* Java version 8 
* [JavalibCore](https://github.com/robotframework/JavalibCore) library. 

In addition it requires appropriate JMS library jar-files are added into class path. E.g. when testing against activeMQ then _activemq-client.jar_ or _activemq-all.jar_ need to be added into class path. 

### Usage

Using library with Maven Robot Framework plug-in either by adding jar file directly or by dependency.

```xml
<plugin>
	<groupId>org.robotframework</groupId>
	<artifactId>robotframework-maven-plugin</artifactId>
	<configuration>
		<extraPathDirectories>
			<extraPathDirectory>path/to/jms-library.jar</extraPathDirectory>
		</extraPathDirectories>
	</configuration>
	<dependencies>
      	<dependency>
	  		<groupId>org.robotframework</groupId>
			<artifactId>javalib-core</artifactId>
 	 	</dependency>
 	 	<dependency>
    		<groupId>org.apache.activemq</groupId>
    		<artifactId>activemq-all</artifactId>
		</dependency>
    </dependencies>
</plugin>
```

```xml
<plugin>
	<groupId>org.robotframework</groupId>
	<artifactId>robotframework-maven-plugin</artifactId>
	<dependencies>
      	<dependency>
        	<groupId>net.relaysoft.robotframework</groupId>
        	<artifactId>jms-library</artifactId>
      	</dependency>
      	<dependency>
	  		<groupId>org.robotframework</groupId>
			<artifactId>javalib-core</artifactId>
 	 	</dependency>
 	 	<dependency>
    		<groupId>org.apache.activemq</groupId>
    		<artifactId>activemq-all</artifactId>
		</dependency>
    </dependencies>
</plugin>
```

Using library with Robot Framework standalone JAR distribution.

	java -cp jms-library.jar:activemq-all.jar:javalib-core.jar:robotframework.jar org.robotframework.RobotFramework test.robot


## Running the tests

Project unit test and robot keyword tests are executed within Maven _test_ and _verify_ phases.

Unit tests:

	mvn test
	
Integration tests (Robot keyword unit tests):

	mvn verify

## Built With

[Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting pull requests to us.

## Versioning

Uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/robotframework/robotLibrary-JMS/tags). 

## Authors

* **Tapani Leskinen** - [relaysoft.net](https://relaysoft.net)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details

## Maven Dependency

```xml
<dependency>
	<groupId>net.relaysoft.robotframework</groupId>
	<artifactId>jms-library</artifactId> 
	<version>1.0.0</version>
</dependency>
```
