import org.robotframework.javalib.library.AnnotationLibrary;

/**
 * Robot framework JMS library.
 * 
 * @author relaysoft.net
 *
 */
public class JMS extends AnnotationLibrary{
	
	public static final String ROBOT_LIBRARY_SCOPE = "TEST SUITE";
	public static final String ROBOT_LIBRARY_VERSION = "1.0.0";
	
	private static final String INTRO = "Robot Framework test library for executing JMS operations."
			+ "\n\n" 
            + "_JMS_ is a test library for executing JMS operations from Java JMS API versions 1.1 and 2.x. Usage requirres "
            + "to add MQ broker specific JMS implementation libraries in robot framework classpath. E.g. when testing against "
            + "Active MQ then Active MQ JMS implementation JAR must be provided. By default connection is established through "
            + "JNDI parameters _java.naming.factory.initial_ and _java.naming.provider.url_ which work when used e.g. with "
            + "Active MQ libraries. If MQ broker specific JMS implementation library does not support the usage of basic JNDI "
            + "properties or there is a need for using library specific extensions for connection, then it is also possible to "
            + "use broker provider specific implementation for JMS client initialization. Currently JMS library supports "
            + "_ACTIVE MQ_ and _IBM MQ_ provider specific implementations."
            + "\n\n" 
			+ "Currently library does not utilize any JMS 2.0 only specific operations but can use JMS 2.x implementations " 
            + "libraries like Apache ActiveMQ artemis project."
            + "\n\n"
            + "The library has the following main usages:\n\n"
            + "- Reading messages from MQ server using JMS\n"
            + "- Writing messages into MQ server using JMS\n"
            + "- Querying queues meta information using JMS"
            + "\n\n"
            + "== Connecting MQ server =="
            + "\n\n"
            + "Connecting into MQ servers is done by first initializing new JMS client with `Initialize Client` keyword and "
            + "then using `Open Connection` for opening the the actual connection."
            + "\n\n"
            + "Example:\n"
            + "| Suite Setup | Initialize Client | clientID=robotClient | parameters=${PARAMETERS_MAP} | version=1 | MQProviderName=ACTIVEMQ |\n"
            + "| Open Connection | | | |\n"
            + "| ... | | | |\n"
            + "| Suite Teardown | Close Connection | | | | |\n\n";
	
	public JMS() {
        super("net/relaysoft/robot/jms/keywords/*.class");
    }
 
    @Override
    public String getKeywordDocumentation(String keywordName) {
        if (keywordName.equals("__intro__")) {
            return INTRO;
        }
        return super.getKeywordDocumentation(keywordName);
    }
    
}
