package net.relaysoft.robot.jms.exceptions;

/**
 * Generic JMS client exception.
 * 
 * @author relaysoft.net
 *
 */
public class JMSClientException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JMSClientException() {}

	public JMSClientException(String message) {
		super(message);
	}

	public JMSClientException(Throwable cause) {
		super(cause);
	}

	public JMSClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public JMSClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
