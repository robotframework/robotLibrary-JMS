package net.relaysoft.robot.jms.keywords;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.jms.JMSException;

import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywordOverload;
import org.robotframework.javalib.annotation.RobotKeywords;

import net.relaysoft.robot.jms.client.JMSClient;
import net.relaysoft.robot.jms.client.JMSClientFactory;
import net.relaysoft.robot.jms.utils.MessageTypeEnum;
import net.relaysoft.robot.jms.utils.PrintUtil;
import net.relaysoft.robot.jms.utils.StringUtils;

@RobotKeywords
public class JMSKeywords {

	protected static final long DEFAULT_TIMEOUT = 2000;

	private static final String LATEST = "latest";

	private JMSClient client;

	@RobotKeyword("Clear all messages stored in client internal cache."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Clear Messages |")
	public void clearMessages(){
		client.clearMessages();
	}

	@RobotKeyword("Close connection to connected MQ-broker. If client is not connected then this keyword has no any affect."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Close Connection |")
	public void closeConnection() throws JMSException{
		client.closeConnection();
	}

	@RobotKeyword("Creates new temporary queue into remote broker. Return name of the temporary queue."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Create Temporary Queue |")
	public String createTemporaryQueue() throws JMSException{
		return client.createTemporaryQueue();
	}

	@RobotKeyword("Creates new temporary topic into remote broker. Return name of the temporary topic."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Create Temporary Topic |")
	public String createTemporaryTopic() throws JMSException{
		return client.createTemporaryTopic();
	}

	@RobotKeyword("Deletes existing temporary queue from remote broker."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _queueName_: Temporary queue's name"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Delete Temporary Queue | ${QUEUE_NAME} |")
	@ArgumentNames({"queueName"})
	public void deleteTemporaryQueue(String queueName) throws JMSException{
		PrintUtil.printOut("Deleting temporary queue: " + queueName);
		client.deleteTemporaryQueue(queueName);
	}

	@RobotKeyword("Deletes existing temporary topic from remote broker."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _topicName_: Temporary topic's name"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Delete Temporary Topic | ${TOPIC_NAME} |")
	@ArgumentNames({"topicName"})
	public void deleteTemporaryTopic(String topicName) throws JMSException{
		PrintUtil.printOut("Deleting temporary topic: " + topicName);
		client.deleteTemporaryTopic(topicName);
	}

	@RobotKeyword("Return byte content for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the content of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID\n"
			+ "- filePath: Optional path to file for saving message content"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Byte Content | messageID=${MESSAGE_ID} | filePath=${FILE_PATH} |\n"
			+ "| Get Message Byte Content | ${MESSAGE_ID} |\n"
			+ "| Get Message Byte Content | filePath=${FILE_PATH} |")
	@ArgumentNames({"messageID=", "filePath="})
	public byte[] getMessageByteContent(String messageID, String filePath) throws JMSException{
		PrintUtil.printOut("Getting byte content from message: " + (messageID != null ? messageID : LATEST));
		byte[] content = client.getMessageByteContent(messageID);
		if(StringUtils.isNotBlank(filePath)){
			writeToFile(filePath, content);
		}
		return content;
	}

	@RobotKeywordOverload
	public byte[] getMessageByteContent(String messageID) throws JMSException{
		return getMessageByteContent(messageID, null);
	}

	@RobotKeywordOverload
	public byte[] getMessageByteContent() throws JMSException{
		return getMessageByteContent(null, null);
	}

	@RobotKeyword("Return ID of the current JMS client initialized by this library."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Client ID |")
	public String getClientID(){
		return client.getClientID();
	}

	@RobotKeyword("Return map content for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the content of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Map Content | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public Map<String, Object> getMessageMapContent(String messageID) throws JMSException{
		PrintUtil.printOut("Getting map content from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageMapContent(messageID);
	}

	@RobotKeywordOverload
	public Map<String, Object> getMessageMapContent() throws JMSException{
		return getMessageMapContent(null);
	}

	@RobotKeyword("Return correlation ID for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the correlation ID of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Correlation ID | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public String getMessageCorrelationID(String messageID) throws JMSException{
		PrintUtil.printOut("Getting correlation ID from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageCorrelationID(messageID);
	}

	@RobotKeywordOverload
	public String getMessageCorrelationID() throws JMSException{
		return getMessageCorrelationID(null);
	}

	@RobotKeyword("Return destination name for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the destination name of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Destination Name | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public String getMessageDestinationName(String messageID) throws JMSException{
		PrintUtil.printOut("Getting destination name from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageDestinationName(messageID);
	}

	@RobotKeywordOverload
	public String getMessageDestinationName() throws JMSException{
		return getMessageDestinationName(null);
	}

	@RobotKeyword("Return expiration time for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the expiration time of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Expiration Time | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public long getMessageExpirationTime(String messageID) throws JMSException{
		PrintUtil.printOut("Getting expiration time from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageExpirationTime(messageID);
	}

	@RobotKeywordOverload
	public long getMessageExpirationTime() throws JMSException{
		return getMessageExpirationTime(null);
	}

	@RobotKeyword("Return ID for the most recent message read from queue/topic."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message ID | |")
	@ArgumentNames({"messageID="})
	public String getMessageID() throws JMSException{
		return client.getMessageID(null);
	}

	@RobotKeyword("Return priority for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the priority of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Priority | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public int getMessagePriority(String messageID) throws JMSException{
		PrintUtil.printOut("Getting priority from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessagePriority(messageID);
	}

	@RobotKeywordOverload
	public int getMessagePriority() throws JMSException{
		return getMessagePriority(null);
	}

	@RobotKeyword("Return property value for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the property value of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _propertyName_: Property name\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Property | ${PROPERTY_NAME} | ${MESSAGE_ID} |\n"
			+ "| Get Message Property | ${PROPERTY_NAME} | |")
	@ArgumentNames({"propertyName", "messageID="})
	public Object getMessageProperty(String propertyName, String messageID) throws JMSException{
		PrintUtil.printOut("Getting message property '" + propertyName + "' value from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageProperty(propertyName, messageID);
	}

	@RobotKeywordOverload
	public Object getMessageProperty(String propertyName) throws JMSException{
		return getMessageProperty(propertyName, null);
	}

	@RobotKeyword("Return reply to destination name for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the reply to destination name of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Reply To Queue | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public String getMessageReplyToQueue(String messageID) throws JMSException{
		PrintUtil.printOut("Getting reply to queue from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageReplyToQueue(messageID);
	}

	@RobotKeywordOverload
	public String getMessageReplyToQueue() throws JMSException{
		return getMessageReplyToQueue(null);
	}

	@RobotKeyword("Return timestamp for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the timestamp of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Timestamp | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public long getMessageTimestamp(String messageID) throws JMSException{
		PrintUtil.printOut("Getting timestamp from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageTimestamp(messageID);
	}

	@RobotKeywordOverload
	public long getMessageTimestamp() throws JMSException{
		return getMessageTimestamp(null);
	}

	@RobotKeyword("Return JMS type for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the JMS type of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Type | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public String getMessageType(String messageID) throws JMSException{
		PrintUtil.printOut("Getting type from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageType(messageID);
	}

	@RobotKeywordOverload
	public String getMessageType() throws JMSException{
		return getMessageType(null);
	}

	@RobotKeyword("Return text content for the message read from queue/topic. If message ID is not given then keyword will "
			+ "return the content of the most recent message read from the queue/topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _messageID_: Message's ID"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Message Text Content | ${MESSAGE_ID} |")
	@ArgumentNames({"messageID="})
	public String getMessageTextContent(String messageID) throws JMSException{
		PrintUtil.printOut("Getting text content from message: " + (messageID != null ? messageID : LATEST));
		return client.getMessageTextContent(messageID);
	}

	@RobotKeywordOverload
	public String getMessageTextContent() throws JMSException{
		return getMessageTextContent(null);
	}

	@RobotKeyword("Return amount of messages in queue."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the queue to get the depth"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Get Queue Depth | ${QUEUE_NAME} |")
	@ArgumentNames({"queueName"})
	public Integer getQueueDepth(String queueName) throws JMSException{
		PrintUtil.printOut("Depth for the queue: " + queueName);
		return client.getQueueDepth(queueName);
	}

	@RobotKeyword("Initialize JMS client. This keyword need to be executed before any others can be used. "
			+ "Calling this keyword more than once leads to override of previous client unless it is currently connected, "
			+ "in which case calling this keyword has no affect."
			+ "\n\n"
			+ "Connection specific parameters are given in dictionary variable. In case of default JNDI provider dictionary "
			+ "must contain needed JNDI key value pairs.\n"
			+ "Example parameter dictionary content for default JNDI provider:\n"
			+ "| java.naming.factory.initial=org.apache.activemq.jndi.ActiveMQInitialContextFactory | java.naming.provider.url=tcp://localhost:61616 |"
			+ "\n\n"
			+ "In case of Active MQ specific provider dictionary must contain needed Active MQ connection factory specific variables."
			+ "\n\n"
			+ "Supported values are:\n"
			+ "- _brokerUrl_: Contains broker URL information. Check detailed information from [http://activemq.apache.org|Active MQ] website."
			+ "\n\n"
			+ "Example parameter dictionary content for Active MQ provider:\n"
			+ "| brokerUrl=tcp://localhost:61616 |"
			+ "\n\n"
			+ "In case of IBM MQ specific provider dictionary must contain needed IBM MQ connection factory specific variables."
			+ "\n\n"
			+ "Supported values are:\n"
			+ "- _hostName_: Broker host name.\n"
			+ "- _channel_: Broker channel used for the client.\n"
			+ "- _port_: Port number.\n"
			+ "- _queueManager_: Queue manager name.\n"
			+ "- _transportType_: Transport type e.g. _CLIENT_ number (1) or _BINDINGS_ number (2)."
			+ "\n\n"
			+ "Example parameter dictionary content for IBM MQ provider:\n"
			+ "| hostName=localhost | channel=testChannel | port=${4444} | queueManager=testQueueManager | transportType=${1} |"
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _clientID_: Unique ID for the JMS client\n"
			+ "- _parameters_: Map (dictionary) of parameters used for client initialization.\n"
			+ "- _version_: Optional JMS API version. Valid values are number 1 (1.1) or 2 (2.x). By default value 1 is used.\n"
			+ "- _providerName_: Optional provider name if provider specific connection factory need to be used. Currently supported "
			+ "values are '_ACTIVEMQ_' and '_IBMMQ_'. If not given then default JNDI connection factory will be used."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Initialize Client | clientID=robotClient | parameters=${PARAMETERS} | providerName=ACTIVEMQ | version=${1} |\n"
			+ "| Initialize Client | clientID=robotClient | parameters=${PARAMETERS} | providerName=ACTIVEMQ | |\n"
			+ "| Initialize Client | clientID=robotClient | parameters=${PARAMETERS} | | |")
	@ArgumentNames({"clientID", "parameters", "providerName=", "version="})
	public void initializeClient(String clientID, Map<String, Object> parameters, String providerName, Integer version) throws Exception{
		PrintUtil.printOut("Initializing new JMS version: " + version + " client with name '" + clientID + "' for " 
				+ (providerName != null ? providerName : "default") + " provider with paramters: " + parameters.toString());
		client = JMSClientFactory.getJMSClient(clientID, version, providerName);
		client.initializeClient(parameters);
	}

	@RobotKeywordOverload
	public void initializeClient(String clientID, Map<String, Object> parameters, String providerName) throws Exception{
		initializeClient(clientID, parameters, providerName, 1);
	}

	@RobotKeywordOverload
	public void initializeClient(String clientID, Map<String, Object> parameters) throws Exception{
		initializeClient(clientID, parameters, null, 1);
	}

	@RobotKeyword("Initialize JMS client. This keyword need to be executed before any others can be used. "
			+ "Calling this keyword more than once leads to override of previous client unless it is currently connected, "
			+ "in which case calling this keyword has no affect."
			+ "\n\n"
			+ "Connection specific parameters are given in external property file. In case of default JNDI provider property file "
			+ "must contain needed JNDI properties.\n"
			+ "Example property file content for default JNDI provider:\n"
			+ "| java.naming.factory.initial=org.apache.activemq.jndi.ActiveMQInitialContextFactory |\n"
			+ "| java.naming.provider.url=tcp://localhost:61616 |"
			+ "\n\n"
			+ "In case of Active MQ specific provider property file must contain needed Active MQ connection factory specific variables."
			+ "\n\n"
			+ "Supported values are:\n"
			+ "- _brokerUrl_: Contains broker URL information. Check detailed information from [http://activemq.apache.org|Active MQ] website."
			+ "\n\n"
			+ "Example property file content for Active MQ provider:\n"
			+ "| brokerUrl=tcp://localhost:61616 |"
			+ "\n\n"
			+ "In case of IBM MQ specific provider property file must contain needed IBM MQ connection factory specific variables."
			+ "\n\n"
			+ "Supported values are:\n"
			+ "- _hostName_: Broker host name.\n"
			+ "- _channel_: Broker channel used for the client.\n"
			+ "- _port_: Port number.\n"
			+ "- _queueManager_: Queue manager name.\n"
			+ "- _transportType_: Transport type e.g. _CLIENT_ number (1) or _BINDINGS_ number (2)."
			+ "\n\n"
			+ "Example property file content for IBM MQ provider:\n"
			+ "| hostName=localhost |\n"
			+ "| channel=testChannel |\n"
			+ "| port=4444 |\n"
			+ "| queueManager=testQueueManager |\n"
			+ "| transportType=1 |"
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _clientID_: Unique ID for the JMS client\n"
			+ "- _propertiesFile_: Path to external properties file containing provider specific configuration.\n"
			+ "- _version_: Optional JMS API version. Valid values are number 1 (1.1) or 2 (2.x). By default value 1 is used.\n"
			+ "- _providerName_: Optional provider name if provider specific connection factory need to be used. Currently supported "
			+ "values are '_ACTIVEMQ_' and '_IBMMQ_'. If not given then default JNDI connection factory will be used."
			+ "\n\n"
			+ "Examples:\n"
			+ "| Initialize Client From File | clientID=robotClient | propertiesFile=/path/to/file | providerName=ACTIVEMQ | version=${1} |\n"
			+ "| Initialize Client From File | clientID=robotClient | propertiesFile=/path/to/file | providerName=ACTIVEMQ | |\n"
			+ "| Initialize Client From File | clientID=robotClient | propertiesFile=/path/to/file | | |")
	@ArgumentNames({"clientID", "propertiesFile", "providerName=", "version="})
	public void initializeClientFromFile(String clientID, String propertiesFile, String providerName, Integer version) throws Exception{
		PrintUtil.printOut("Initializing new JMS version: " + version + " client with name '" + clientID + "' for " 
				+ (providerName != null ? providerName : "default") + " provider with paramters from file: " + propertiesFile);
		client = JMSClientFactory.getJMSClient(clientID, version, providerName);
		client.initializeClient(convertProperties2Map(loadProperties(propertiesFile)));
	}

	@RobotKeywordOverload
	public void initializeClientFromFile(String clientID, String propertiesFile, String providerName) throws Exception{
		initializeClientFromFile(clientID, propertiesFile, providerName, 1);
	}
	
	@RobotKeywordOverload
	public void initializeClientFromFile(String clientID, String propertiesFile) throws Exception{
		initializeClientFromFile(clientID, propertiesFile, null, 1);
	}

	@RobotKeyword("Open client connection to message broker. Client must be initialized with keyword `initializeClient` or "
			+ "`initializeClientFromFile` before connecting. After connecting all the other keywords can be used."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _userName_: Optional username for authentication if requirred by the broker\n"
			+ "- _password_: Optional password for authentication if requirred by the broker"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Open Connection | userName=${USERNAME} | password=${PASSWORD} |\n"
			+ "| Open Connection | | |")
	@ArgumentNames({"userName=", "password="})
	public void openConnection(String userName, String password) throws JMSException{
		if(userName != null && password != null){
			PrintUtil.printOut("Opening connection with username and password");
		}
		client.openConnection(userName, password);
	}

	@RobotKeywordOverload
	public void openConnection() throws JMSException{
		openConnection(null, null);
	}

	@RobotKeyword("Publishes new JMS message into topic."
			+ "\n\n"
			+ "Publish keyword can use certain JMS parameters when publishing the message given in _parameters_ variable.\n"
			+ "Following parameters can be given:\n"
			+ "- _deliveryDelay_: Delay for the delivery in milliseconds\n"
			+ "- _deliveryMode_: Message delivery mode _PERSISTENT_ (1) or _NON PERSISTENT_ (0)\n"
			+ "- _correlationId_: Correlation ID for the message. E.g. when using _replyTo_ parameter\n"
			+ "- _replyTo_: Reply to destination name\n"
			+ "- _type_: JMS type\n"
			+ "- _priority_: Message's priority. Numeric value between 1-9\n"
			+ "- _timeToLive_: Time to live in milliseconds\n"
			+ "Arguments:\n"
			+ "- _topicName_: Name of the topic to publish\n"
			+ "- _content_: JMS Message content. Can be String, dictionary, binary data\n"
			+ "- _parameters_: Optional dictionary of JMS parameter key value pairs used for publishing\n"
			+ "- _properties_: Optional dictionary of JMS message property key value pairs to be set for the published message"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Publish Into Topic | topicName=testTopic | content=Test content | parameters=${PARAMETERS} | properties=${PROPERTIES} |\n"
			+ "| Publish Into Topic | topicName=testTopic | content=Test content | parameters=${PARAMETERS} | |\n"
			+ "| Publish Into Topic | topicName=testTopic | content=Test content | | |")
	@ArgumentNames({"topicName", "content=", "parameters=", "properties="})
	@SuppressWarnings("unchecked")
	public void publishIntoTopic(String topicName, Object content, Map<String, Object> parameters, 
			Map<String, Object> properties) throws JMSException{
		PrintUtil.printOut("Publish new message into topic '" + topicName + "' with content: " + content.toString());
		printOutMessageParamsAndProps(parameters, properties);
		if(content instanceof String){
			client.publishIntoTopic(topicName, (String) content, parameters, properties);
		} else if(content instanceof Map){
			client.publishIntoTopic(topicName, (Map<String, Object>) content, parameters, properties);
		} else if(content instanceof byte[]){
			client.publishIntoTopic(topicName, (byte[]) content, parameters, properties);
		} else {
			throw new JMSException("Unsupported JMS message content type: " + content.getClass().getName());
		}
	}

	@RobotKeywordOverload
	public void publishIntoTopic(String topicName, Object content, Map<String, Object> parameters) throws JMSException{
		publishIntoTopic(topicName, content, parameters, null);
	}

	@RobotKeywordOverload
	public void publishIntoTopic(String topicName, Object content) throws JMSException{
		publishIntoTopic(topicName, content, null, null);
	}

	@RobotKeyword("Publishes new JMS message into topic with content from external file."
			+ "\n\n"
			+ "Publish keyword can use certain JMS parameters when publishing the message given in _parameters_ variable.\n"
			+ "Following parameters can be given:\n"
			+ "- _deliveryDelay_: Delay for the delivery in milliseconds\n"
			+ "- _deliveryMode_: Message delivery mode _PERSISTENT_ (1) or _NON PERSISTENT_ (0)\n"
			+ "- _correlationId_: Correlation ID for the message. E.g. when using _replyTo_ parameter\n"
			+ "- _replyTo_: Reply to destination name\n"
			+ "- _type_: JMS type\n"
			+ "- _priority_: Message's priority. Numeric value between 1-9\n"
			+ "- _timeToLive_: Time to live in milliseconds\n"
			+ "Arguments:\n"
			+ "- _topicName_: Name of the topic to publish\n"
			+ "- _filePath_: Path to file which contains content for the JSM message\n"
			+ "- _type_: Content type for the JMS message: _TEXT_, _BYTE_ or _MAP_. _BYTE_ is default value\n"
			+ "- _parameters_: Optional dictionary of JMS parameter key value pairs used for publishing\n"
			+ "- _properties_: Optional dictionary of JMS message property key value pairs to be set for the published message"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Publish Into Topic From File | topicName=testTopic | filePath=/path/to/file | type=BYTE | parameters=${PARAMETERS} | properties=${PROPERTIES} |\n"
			+ "| Publish Into Topic From File | topicName=testTopic | filePath=/path/to/file | type=BYTE | parameters=${PARAMETERS} | |\n"
			+ "| Publish Into Topic From File | topicName=testTopic | filePath=/path/to/file | type=BYTE | | |\n"
			+ "| Publish Into Topic From File | topicName=testTopic | filePath=/path/to/file | | | |")
	@ArgumentNames({"topicName", "filePath", "type=", "parameters=", "properties="})
	public void publishIntoTopicFromFile(String topicName, String filePath, String type, Map<String, Object> parameters, 
			Map<String, Object> properties) throws IOException, JMSException{
		PrintUtil.printOut("Publish new message into topic '" + topicName + "' with content from file: " + filePath);
		printOutMessageParamsAndProps(parameters, properties);
		byte[] byteContent = Files.readAllBytes(Paths.get(filePath));
		if(StringUtils.isNotBlank(type) && type.equalsIgnoreCase(MessageTypeEnum.TEXT.toString())){
			client.publishIntoTopic(topicName, new String(byteContent, StandardCharsets.UTF_8), parameters, properties);
		} else {
			client.publishIntoTopic(topicName, byteContent, parameters, properties);
		}
	}

	@RobotKeywordOverload
	public void publishIntoTopicFromFile(String topicName, String filePath, String type, Map<String, Object> parameters) 
			throws IOException, JMSException{
		publishIntoTopicFromFile(topicName, filePath, type, parameters, null);
	}
	
	@RobotKeywordOverload
	public void publishIntoTopicFromFile(String topicName, String filePath, String type) throws IOException, JMSException{
		publishIntoTopicFromFile(topicName, filePath, type, null, null);
	}

	@RobotKeywordOverload
	public void publishIntoTopicFromFile(String topicName, String filePath) throws IOException, JMSException{
		publishIntoTopicFromFile(topicName, filePath, null, null, null);
	}

	@RobotKeyword("Delete all messages from queue.\n"
			+ "\n\n"
			+ "NOTE: Queue purification is made by JMS API so there may be a delay after successfull keyword execution before the queue is actually empty."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the queue to purge"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Purge Queue | testQueue |")
	@ArgumentNames({"queueName"})
	public void purgeQueue(String queueName) throws JMSException{
		PrintUtil.printOut("Purge all messages from queue: " + queueName);
		client.purgeQueue(queueName);
	}

	@RobotKeyword("Read single JMS message from queue. Returns message ID. If there are no messages awailable in queue return NULL."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the queue from which message is read from\n"
			+ "- _timeout_: Optional timeout in milliseconds to wait for message to be awailable for reading. Default value is 2000 milliseconds\n"
			+ "- _messageSelector_: Optional message selector string"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Read From Queue | queueName=testQueue | timeout=${2000} | messageSelector=${MESSAGE_SELECTOR} |\n"
			+ "| Read From Queue | queueName=testQueue | timeout=${2000} | |\n"
			+ "| Read From Queue | queueName=testQueue | | |")
	@ArgumentNames({"queueName", "timeout=", "messageSelector="})
	public String readFromQueue(String queueName, Long timeout, String messageSelector) throws JMSException{
		PrintUtil.printOut("Read message from queue '" + queueName + "' with timeout value of " + (timeout != null ? timeout : DEFAULT_TIMEOUT) + " milliseconds");
		if( messageSelector != null){
			PrintUtil.printOut("Using message selector value: " + messageSelector);
		}
		return client.readFromQueue(queueName, (timeout != null ? timeout : DEFAULT_TIMEOUT), messageSelector);
	}

	@RobotKeywordOverload
	public String readFromQueue(String queueName, Long timeout) throws JMSException{
		return readFromQueue(queueName, timeout, null);
	}

	@RobotKeywordOverload
	public String readFromQueue(String queueName) throws JMSException{
		return readFromQueue(queueName, DEFAULT_TIMEOUT, null);
	}

	@RobotKeyword("Read single JMS message from topic. Returns message ID. If there are no messages awailable in topic return NULL "
			+ "There must be active subscription made into topic with `Subscribe Topic` keyword before messages can be read or topic "
			+ "must be temporary type."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the durable subscription or \n"
			+ "- _timeout_: Optional timeout in milliseconds to wait for message to be awailable for reading. Default value is 2000 milliseconds\n"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Read From Topic | name=testSubscription | timeout=${2000} |\n"
			+ "| Read From Topic | name=testSubscription | |")
	@ArgumentNames({"name", "timeout="})
	public String readFromTopic(String name, Long timeout) throws JMSException{
		PrintUtil.printOut("Read message from topic/subscription '" + name + "' with timeout value of " + (timeout != null ? timeout : DEFAULT_TIMEOUT) + " milliseconds");
		return client.readFromTopic(name, (timeout != null ? timeout : DEFAULT_TIMEOUT));
	}

	@RobotKeywordOverload
	public String readFromTopic(String name) throws JMSException{
		return readFromTopic(name, DEFAULT_TIMEOUT);
	}

	@RobotKeyword("Create durable subscription into topic."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _topicName_: Name of the topic to subscribe\n"
			+ "- _name_: Unique name for the subscription\n"
			+ "- _messageSelector_: Optional message selector string"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Subscribe Topic | topicName=testQueue | name=testSubscription | messageSelector=${MESSAGE_SELECTOR} |\n"
			+ "| Subscribe Topic | topicName=testQueue | name=testSubscription | |")
	@ArgumentNames({"topicName", "name", "messageSelector="})
	public void subscribeTopic(String topicName, String name, String messageSelector) throws JMSException{
		PrintUtil.printOut("Creating subcription into topic '" + topicName + "' with name: " + name);
		if( messageSelector != null){
			PrintUtil.printOut("Using message selector value: " + messageSelector);
		}
		client.subscribeTopic(topicName, name, messageSelector);
	}

	@RobotKeywordOverload
	public void subscribeTopic(String topicName, String name) throws JMSException{
		subscribeTopic(topicName, name, null);
	}

	@RobotKeyword("Cancel topic subscription."
			+ "\n\n"
			+ "Arguments:\n"
			+ "- _name_: Subscription name\n"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Unsubscribe Topic | testSubscription |")
	@ArgumentNames({"name"})
	public void unsubscribeTopic(String name) throws JMSException{
		PrintUtil.printOut("Unsubscribe: " + name);
		client.unsubscribeTopic(name);
	}

	@RobotKeyword("Writes new JMS message into queue."
			+ "\n\n"
			+ "Write keyword can use certain JMS parameters when writing the message given in _parameters_ variable.\n"
			+ "Following parameters can be given:\n"
			+ "- _deliveryDelay_: Delay for the delivery in milliseconds\n"
			+ "- _deliveryMode_: Message delivery mode _PERSISTENT_ (1) or _NON PERSISTENT_ (0)\n"
			+ "- _correlationId_: Correlation ID for the message. E.g. when using _replyTo_ parameter\n"
			+ "- _replyTo_: Reply to destination name\n"
			+ "- _type_: JMS type\n"
			+ "- _priority_: Message's priority. Numeric value between 1-9\n"
			+ "- _timeToLive_: Time to live in milliseconds\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the queue to write into\n"
			+ "- _content_: JMS Message content. Can be String, dictionary, binary data\n"
			+ "- _parameters_: Optional dictionary of JMS parameter key value pairs used for writing\n"
			+ "- _properties_: Optional dictionary of JMS message property key value pairs to be set for the message"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Write Into Queue | queueName=testQueue | content=Test content | parameters=${PARAMETERS} | properties=${PROPERTIES} |\n"
			+ "| Write Into Queue | queueName=testQueue | content=Test content | parameters=${PARAMETERS} | |\n"
			+ "| Write Into Queue | queueName=testQueue | content=Test content | | |")
	@ArgumentNames({"queueName", "content", "parameters=", "properties="})
	@SuppressWarnings("unchecked")
	public void writeIntoQueue(String queueName, Object content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException{
		PrintUtil.printOut("Write new message into queue '" + queueName + "' with content: " + content.toString());
		printOutMessageParamsAndProps(parameters, properties);
		if(content instanceof String){
			client.writeIntoQueue(queueName, (String) content, parameters, properties);
		} else if(content instanceof Map){
			client.writeIntoQueue(queueName, (Map<String, Object>) content, parameters, properties);
		} else if(content instanceof byte[]){
			client.writeIntoQueue(queueName, (byte[]) content, parameters, properties);
		} else {
			throw new JMSException("Unsupported JMS message content type: " + content.getClass().getName());
		}
	}

	@RobotKeywordOverload
	public void writeIntoQueue(String queueName, Object content, Map<String, Object> parameters) throws JMSException{
		writeIntoQueue(queueName, content, parameters, null);
	}

	@RobotKeywordOverload
	public void writeIntoQueue(String queueName, Object content) throws JMSException{
		writeIntoQueue(queueName, content, null, null);
	}

	@RobotKeyword("Writes new JMS message into queue with content from external file."
			+ "\n\n"
			+ "Write keyword can use certain JMS parameters when writing the message given in _parameters_ variable.\n"
			+ "Following parameters can be given:\n"
			+ "- _deliveryDelay_: Delay for the delivery in milliseconds\n"
			+ "- _deliveryMode_: Message delivery mode _PERSISTENT_ (1) or _NON PERSISTENT_ (0)\n"
			+ "- _correlationId_: Correlation ID for the message. E.g. when using _replyTo_ parameter\n"
			+ "- _replyTo_: Reply to destination name\n"
			+ "- _type_: JMS type\n"
			+ "- _priority_: Message's priority. Numeric value between 1-9\n"
			+ "- _timeToLive_: Time to live in milliseconds\n"
			+ "Arguments:\n"
			+ "- _queueName_: Name of the topic to write into\n"
			+ "- _filePath_: Path to file which contains content for the JSM message\n"
			+ "- _type_: Content type for the JMS message: _TEXT_, _BYTE_ or _MAP_. _BYTE_ is default value\n"
			+ "- _parameters_: Optional dictionary of JMS parameter key value pairs used for writing\n"
			+ "- _properties_: Optional dictionary of JMS message property key value pairs to be set for the message"
			+ "\n\n"
			+ "Examples:\n"
			+ "| Write Into Queue From File | queueName=testQueue | filePath=/path/to/file | type=BYTE | parameters=${PARAMETERS} | properties=${PROPERTIES} |\n"
			+ "| Write Into Queue From File | queueName=testQueue | filePath=/path/to/file | type=BYTE | parameters=${PARAMETERS} | |\n"
			+ "| Write Into Queue From File | queueName=testQueue | filePath=/path/to/file | type=BYTE | | |\n"
			+ "| Write Into Queue From File | queueName=testQueue | filePath=/path/to/file | | | |")
	@ArgumentNames({"queueName", "filePath", "type=", "parameters=", "properties="})
	public void writeIntoQueueFromFile(String queueName, String filePath, String type, Map<String, Object> parameters, 
			Map<String, Object> properties) throws IOException, JMSException{
		PrintUtil.printOut("Write new message into queue '" + queueName + "' with content from file: " + filePath);
		printOutMessageParamsAndProps(parameters, properties);
		byte[] byteContent = Files.readAllBytes(Paths.get(filePath));
		if(StringUtils.isNotBlank(type) && type.equalsIgnoreCase(MessageTypeEnum.TEXT.toString())){
			client.writeIntoQueue(queueName, new String(byteContent, StandardCharsets.UTF_8), parameters, properties);
		} else {
			client.writeIntoQueue(queueName, byteContent, parameters, properties);
		}
	}
	
	@RobotKeywordOverload
	public void writeIntoQueueFromFile(String queueName, String filePath, String type, Map<String, Object> parameters) 
			throws IOException, JMSException{
		writeIntoQueueFromFile(queueName, filePath, type, parameters, null);
	}

	@RobotKeywordOverload
	public void writeIntoQueueFromFile(String queueName, String filePath, String type) throws IOException, JMSException{
		writeIntoQueueFromFile(queueName, filePath, type, null, null);
	}

	@RobotKeywordOverload
	public void writeIntoQueueFromFile(String queueName, String filePath) throws IOException, JMSException{
		writeIntoQueueFromFile(queueName, filePath, null, null, null);
	}

	/**
	 * Convert {@link Properties} into {@link Map} object. 
	 * 
	 * @param properties - Properties to convert
	 * @return New Map instance containing properties as key value pairs.
	 */
	private Map<String, Object> convertProperties2Map(Properties properties){
		Map<String, Object> map = new HashMap<>();
		for(String name : properties.stringPropertyNames()){
			map.put(name, properties.get(name));
		}
		return map;
	}

	/**
	 * Load properties from file.
	 * 
	 * @param filePath - Properties file path
	 * @return New properties instance generated from properties file.
	 * @throws IOException If properties file is not found or cannot be read for any reason.
	 */
	private Properties loadProperties(String filePath) throws IOException{
		Properties properties = new Properties();
		try(InputStream is = new FileInputStream(filePath)){
			properties.load(is);
		}
		return properties;	
	}

	/**
	 * Print out JMS message parameters and properties.
	 * 
	 * @param parameters - Parameter map
	 * @param properties - Properties map
	 */
	private void printOutMessageParamsAndProps(Map<String, Object> parameters, Map<String, Object> properties){
		if(parameters != null && !parameters.isEmpty()){
			PrintUtil.printOut("Using parameters: " + parameters.toString());
		}
		if(properties != null && !properties.isEmpty()){
			PrintUtil.printOut("Adding message properties: " + properties.toString());
		}
	}

	/**
	 * Write content into given file.
	 * 
	 * @param filePath - Path to file
	 * @param content - File content
	 */
	private void writeToFile(String filePath, Object content) {
		try{
			Path file = Paths.get(filePath);
			if(content instanceof String){
				Files.write(file, ((String) content).getBytes(StandardCharsets.UTF_8));
			} else {
				Files.write(file, (byte[]) content);
			}	
		} catch (IOException e) {
			PrintUtil.printOut("Failed to write message content into file: " + filePath + ". Reason: " + e.getMessage());
		}
	}

}
