package net.relaysoft.robot.jms.utils;

/**
 * The StringUtils class defines helper methods related to String handling.
 * 
 * @author relaysoft.net
 *
 */
public final class StringUtils {
	
	private StringUtils(){}

	/**
	 * Checks whether string is null or empty. String containing only whitespaces is considered as empty string.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string is <code>null</code> or empty.
	 */
	public static boolean isBlank(String string){
		return (string == null || string.trim().isEmpty());
	}
	
	/**
	 * Checks whether string is not null or empty. String containing only whitespaces is considered as empty string.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if string is not <code>null</code> or empty.
	 */
	public static boolean isNotBlank(String string){
		return (string != null && !string.trim().isEmpty());
	}

}
