package net.relaysoft.robot.jms.utils;

import java.lang.reflect.InvocationTargetException;

/**
 * Utility class for reflection handling.
 * 
 * @author relaysoft.net
 *
 */
public final class ReflectionUtil {
	
	private ReflectionUtil(){}
	
	/**
	 * Create class from class name.
	 * 
	 * @param className - Class name for the class to load
	 * @return Class object of given name.
	 * @throws ClassNotFoundException If class with the given name is not found from class path.
	 */
	public static Class<?> createClass(String className) throws ClassNotFoundException {
		return Class.forName(className);
	}
	
	/**
	 * Construct new object from class object. Class must have default type constructor.
	 * 
	 * @param clazz - Class object from which new instance is created.
	 * @return New object instance of given class type.
	 * @throws NoSuchMethodException If class has no default public constructor.
	 * @throws InvocationTargetException If constructor method cannot be invoked. 
	 * @throws IllegalAccessException If constructor method cannot be called. 
	 * @throws InstantiationException If given class cannot be instantiated
	 */
	public static Object constructObject(Class<?> clazz) 
			throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return clazz.getConstructor(new Class[] {}).newInstance(new Object[] {});
	}
	
	/**
	 * Execute method in object.
	 * 
	 * @param clazz - Object's class type
	 * @param obj - Object containing the method
	 * @param methodName - Name of the method to execute
	 * @param parameterTypes - Method parameter types
	 * @param parameters - Method parameter values
	 * @return The result of dispatching the method.
	 * @throws NoSuchMethodException If given class does not contain requested method.
	 * @throws InvocationTargetException If given method cannot be invoked.
	 * @throws IllegalAccessException If given method cannot be called.
	 */
	public static Object executeMethod(Class<?> clazz, Object obj, String methodName, Class<?>[] parameterTypes, Object[] parameters) 
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return clazz.getDeclaredMethod(methodName, parameterTypes).invoke(obj, parameters);
	}

}
