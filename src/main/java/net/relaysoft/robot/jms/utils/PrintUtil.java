package net.relaysoft.robot.jms.utils;

/**
 * Utility class for printing into robot framework output log.
 * 
 * @author relaysoft.net
 *
 */
public final class PrintUtil {

	private PrintUtil() {}
	
	/**
	 * Print INFO into robot log output.
	 * 
	 * @param string - Value to print
	 */
	public static void printOut(String string){
		if(StringUtils.isNotBlank(string)){
			System.out.println(string);
		}
	}

}
