package net.relaysoft.robot.jms.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for JMS message content handling.
 * 
 * @author relaysoft.net
 *
 */
public final class MessageContentUtils {

	private MessageContentUtils(){}
	
	/**
	 * Converts serialized map byte array into map.
	 * 
	 * @param serialisedMap - Serialized map object to convert
	 * @return Map object.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> convertByteArrayToMap(byte[] serialisedMap){
		ByteArrayInputStream bin = new ByteArrayInputStream(serialisedMap);
	    try(ObjectInputStream in = new ObjectInputStream(bin)){
	    	return (Map<String, Object>) in.readObject();
	    } catch (ClassNotFoundException e) {
	    	// Should never happen!
	    	throw new RuntimeException("Fatal error! Cannot find Map class from classpath. Reason: " + e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException("Failed to write byte message content into map. Reason: " + e.getMessage());
		}
	}
	
	/**
	 * Convert map object into byte array.
	 * 
	 * @param map - Map to convert
	 * @return Map object in byte array.
	 */
	public static byte[] convertMapToByteArray(Map<String, Object> map){
		HashMap<String, Object> tempMap = new HashMap<>();
		tempMap.putAll(map);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
	    try(ObjectOutputStream out = new ObjectOutputStream(bout)){
	    	out.writeObject(tempMap);
	    	return bout.toByteArray();
	    } catch (IOException e) {
			throw new RuntimeException("Failed to write map message content into byte array. Reason: " + e.getMessage());
		}	 
	}

}
