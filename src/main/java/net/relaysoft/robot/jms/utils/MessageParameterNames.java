package net.relaysoft.robot.jms.utils;

/**
 * JMS message parameter names.
 * 
 * @author relaysoft.net
 *
 */
public final class MessageParameterNames {
	
	private MessageParameterNames(){}

	public static final String DELIVERY_DELAY = "deliveryDelay";
	public static final String DELIVERY_MODE = "deliveryMode";
	public static final String CORRELATION_ID = "correlationId";
	public static final String REPLY_TO = "replyTo";
	public static final String TYPE = "type";
	public static final String PRIORITY = "priority";
	public static final String TIME_TO_LIVE = "timeToLive";

}
