package net.relaysoft.robot.jms.client;

import java.util.Map;

import javax.jms.ConnectionFactory;

import net.relaysoft.robot.jms.utils.ReflectionUtil;

/**
 * ActiveMQ provider JMS API 1.1 version specific implementation of JMS client wrapper interface.
 * 
 * @author relaysoft.net
 *
 */
public class ActiveMQ11Client extends AbstractJMS11Client implements JMSClient {
	
	public static final String PARAM_BROKER_URL = "brokerUrl";
	
	private static final String CLASS_CONNECTION_FACTORY = "org.apache.activemq.ActiveMQConnectionFactory";

	/**
	 * Creates new ActiveMQ client instance.
	 * 
	 * @param clientID - Unique client ID
	 */
	public ActiveMQ11Client(String clientID) {
		super(clientID);
	}

	@Override
	protected ConnectionFactory createConnectionFactory(Map<String, Object> parameters) throws Exception {
		Class<?> connectionFactoryClazz = ReflectionUtil.createClass(CLASS_CONNECTION_FACTORY);
		Object connectionFactoryInstance = ReflectionUtil.constructObject(connectionFactoryClazz);
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setBrokerURL", 
				new Class[] {String.class}, new Object[] {parameters.get(PARAM_BROKER_URL)});
		return (ConnectionFactory) connectionFactoryInstance;
	}

}
