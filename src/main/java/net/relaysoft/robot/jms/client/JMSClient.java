package net.relaysoft.robot.jms.client;

import java.util.Map;

import javax.jms.JMSException;

import net.relaysoft.robot.jms.utils.MessageTypeEnum;

/**
 * JMS client for controlling operations against message queue provider.
 * 
 * @author relaysoft.net
 *
 */
public interface JMSClient {
	
	/**
	 * Clear all read messages from client's storage.
	 */
	void clearMessages();
	
	/**
	 * Permanently closes JMS client connection.
	 * 
	 * @throws JMSException If closing of connection fails
	 */
	void closeConnection() throws JMSException;
	
	/**
	 * Create new temporary queue.
	 * 
	 * @return Created temporary queue's name.
	 * @throws JMSException If temporary queue creation fails.
	 */
	String createTemporaryQueue() throws JMSException;
	
	/**
	 * Create new temporary topic.
	 * 
	 * @return Created temporary topic's name
	 * @throws JMSException If temporary topic creation fails.
	 */
	String createTemporaryTopic() throws JMSException;
	
	/**
	 * Deletes temporary queue.
	 * 
	 * @param queueName - Temporary queue name
	 * @throws JMSException If deletion fails.
	 */
	void deleteTemporaryQueue(String queueName) throws JMSException;
	
	/**
	 * Deletes temporary topic.
	 * 
	 * @param topicName  - Temporary topic name
	 * @throws JMSException If deletion fails.
	 */
	void deleteTemporaryTopic(String topicName) throws JMSException;
	
	/**
	 * @return Client's unique ID
	 */
	String getClientID();
	
	/**
	 * Return byte message's byte content.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message 
	 * @return Byte message's byte content. 
	 * @throws JMSException If reading bytes from message fails.  
	 */
	byte[] getMessageByteContent(String messageID) throws JMSException;
	
	/**
	 * Return map message's map content.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Map message's map content.
	 * @throws JMSException If reading map from message fails.
	 */
	Map<String, Object> getMessageMapContent(String messageID) throws JMSException;
	
	/**
	 * Return message's correlation ID
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's correlation ID.
	 * @throws JMSException If getting message's meta data fails.
	 */
	String getMessageCorrelationID(String messageID) throws JMSException;
	
	/**
	 * Return message's target destination (Queue or Topic) name.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's destination name.
	 * @throws JMSException If getting message's meta data fails.
	 */
	String getMessageDestinationName(String messageID) throws JMSException;
	
	/**
	 * Return message's expiration time in milliseconds. 
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's expiration time.
	 * @throws JMSException If getting message's meta data fails.
	 */
	long getMessageExpirationTime(String messageID) throws JMSException;
	
	/**
	 * Return message's unique ID. 
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's ID.
	 * @throws JMSException If getting message's meta data fails.
	 */
	String getMessageID(String messageID) throws JMSException;
	
	/**
	 * Return message's priority value from 1-9.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's priority value.
	 * @throws JMSException If getting message's meta data fails.
	 */
	int getMessagePriority(String messageID) throws JMSException;
	
	/**
	 * Return message property value
	 * 
	 * @param propertyName - Name of the property
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message property value.
	 * @throws JMSException If getting message's meta data fails.
	 */
	Object getMessageProperty(String propertyName, String messageID) throws JMSException;
	
	/**
	 * Return message's reply to queue (or topic) target destination.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's reply to destination name.
	 * @throws JMSException If getting message's meta data fails.
	 */
	String getMessageReplyToQueue(String messageID) throws JMSException;
	
	/**
	 * Return text message's string content.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Text message's content as string.
	 * @throws JMSException - If getting message content fails for any reason.
	 */
	String getMessageTextContent(String messageID) throws JMSException;
	
	/**
	 * Return message's latest timestamp.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's timestamp.
	 * @throws JMSException If getting message's meta data fails.
	 */
	long getMessageTimestamp(String messageID) throws JMSException;
	
	/**
	 * Return message's JMS type.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return Message's type
	 * @throws JMSException If getting message's meta data fails.
	 */
	String getMessageType(String messageID) throws JMSException;
	
	/**
	 * Return specified queue current depth (Amount of unread messages in queue).
	 * 
	 * @param queueName - Name of the queue to check
	 * @return Current queue depth.
	 * @throws JMSException If getting queue depth fails for any reason.
	 */
	int getQueueDepth(String queueName) throws JMSException;

	/**
	 * Initializes current JMS client wrapper. Client need to be initialized before opening any connections. Initialization parameters
	 * can contain any provider specific connection factory initialization variables or JNDI properties for the JMS connection factory.
	 * 
	 * @param parameters - Parameters for client initialization.
	 * @throws Exception If initializing the client fails for any reason.
	 */
	void initializeClient(Map<String, Object> parameters) throws Exception;
	
	/**
	 * Open new connection into MQ provider/broker. If connection already exists, this method call has no any effect.
	 * 
	 * @throws JMSException If opening the connection fails.
	 */
	void openConnection() throws JMSException;
	
	/**
	 * Open new connection into MQ provider/broker. If connection already exists, this method call has no any effect.
	 * 
	 * @param userName - User name used for connection authentication 
	 * @param password - Password used for connection authentication 
	 * @throws JMSException If opening the connection fails.
	 */
	void openConnection(String userName, String password) throws JMSException;
	
	/**
	 * Peek first message from the queue. Peeking message does not remove message from queue.
	 * 
	 * @param queueName - Queue to peek from
	 * @param messageSelector - Optional message selector to filter queue content
	 * @return Message's ID or <code>null</code> if no message was peeked from the queue.
	 * @throws JMSException If reading the message fails.
	 */
	String peekFromQueue(String queueName, String messageSelector) throws JMSException;

	/**
	 * Publish new message into topic.
	 * 
	 * @param topicName - Topic name into which message is published
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or publish it into topic fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames
	 */
	void publishIntoTopic(String topicName, String content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
	/**
	 * Publish new message into topic.
	 * 
	 * @param topicName - Topic name into which message is published
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or publish it into topic fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames 
	 */
	void publishIntoTopic(String topicName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
	/**
	 * Publish new message into topic.
	 * 
	 * @param topicName - Topic name into which message is published
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or publish it into topic fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames
	 */
	void publishIntoTopic(String topicName, Map<String, Object> content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
	/**
	 * Remove all messages from specified queue.
	 * 
	 * @param queueName - Name of the queue to purge from messages
	 * @throws JMSException If purging the queue fails for any reason.
	 */
	void purgeQueue(String queueName) throws JMSException;
	
	/**
	 * Read single message from the queue.
	 * 
	 * @param queueName - Name of the queue to read from
	 * @param timeout - Timeout in milliseconds to wait for message to appear into queue
	 * @param messageSelector - Optional message selector string for filtering messages
	 * @return Message's ID or <code>null</code> if no message was read from the queue.
	 * @throws JMSException If reading the message fails.
	 */
	String readFromQueue(String queueName, long timeout, String messageSelector) throws JMSException;
	
	/**
	 * Read single message from the topic.
	 * 
	 * @param name - Name of the durable subscription made for the topic or name of the temporary topic
	 * @param timeout - Timeout in milliseconds to wait for message to appear into topic
	 * @return Message's ID or <code>null</code> if no message was read from the topic.
	 * @throws JMSException If reading the message fails.
	 */
	String readFromTopic(String name, long timeout) throws JMSException;
	
	/**
	 * Resolve JMS message's type. <code>TEXT</code>, <code>BYTE</code> or <code>MAP</code>.
	 * 
	 * @param messageID - Optional message ID. If not given then method attempt to read content from latest read message
	 * @return JMS message's type.
	 */
	MessageTypeEnum resolveMessageType(String messageID);
	
	/**
	 * Make new durable subscription into topic.
	 * 
	 * @param topicName - Name of the topic
	 * @param name - Unique name for the durable subscription
	 * @param messageSelector - Optional message selector string for filtering messages
	 * @throws JMSException If subscribing the topic fails.
	 */
	void subscribeTopic(String topicName, String name, String messageSelector) throws JMSException;
	
	/**
	 * Cancel made durable subscription for the topic.
	 * 
	 * @param name - Unique subscription name to cancel
	 * @throws JMSException If canceling the subscription fails. 
	 */
	void unsubscribeTopic(String name) throws JMSException;
	
	/**
	 * Write new message into queue
	 * 
	 * @param queueName - Name of the queue to write
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or write it into queue fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames
	 */
	void writeIntoQueue(String queueName, String content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
	/**
	 * Write new message into queue
	 * 
	 * @param queueName - Name of the queue to write
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or write it into queue fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames
	 */
	void writeIntoQueue(String queueName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
	/**
	 * Write new message into queue
	 * 
	 * @param queueName - Name of the queue to write
	 * @param content - Message's content
	 * @param parameters - Optional message's JMS parameters. 
	 * Key values are defined in {@link net.relaysoft.robot.jms.utils.MessageParameterNames MessageParameterNames}
	 * @param properties - Optional JMS message properties
	 * @throws JMSException If message creation or write it into queue fails.
	 * @see net.relaysoft.robot.jms.utils.MessageParameterNames
	 */
	void writeIntoQueue(String queueName, Map<String, Object> content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException;
	
}
