package net.relaysoft.robot.jms.client;

import java.util.Hashtable;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Default JMS version 2 client implementation. This client's connection factory is configured through JNDI parameters.
 * 
 * @author relaysoft.net
 *
 */
public class DefaultJMS20Client extends AbstractJMS20Client implements JMSClient {

	/**
	 * Create new JMS client instance.
	 * 
	 * @param clientID - Unique ID
	 */
	public DefaultJMS20Client(String clientID) {
		super(clientID);
	}

	@Override
	protected ConnectionFactory createConnectionFactory(Map<String, Object> parameters) throws Exception {
		return (ConnectionFactory) createContext(parameters).lookup(CONNECTION_FACTORY_NAME);
	}
	
	/**
	 * Creates new context to lookup for connection factory.
	 * 
	 * @param parameters - JNDI properties
	 * @return Context for JMS connection factory.
	 * @throws NamingException - If context creation fails.
	 */
	private Context createContext(Map<String, Object> parameters) throws NamingException{
		Hashtable<Object, Object> environment = new Hashtable<>();
		environment.putAll(parameters);
		return new InitialContext(environment);
	}

}
