package net.relaysoft.robot.jms.client;

import java.util.Map;

import javax.jms.ConnectionFactory;

import net.relaysoft.robot.jms.utils.ReflectionUtil;

/**
 * IBM MQ provider JMS API 1.1 version specific implementation of JMS client wrapper interface.
 * 
 * @author relaysoft.net
 *
 */
public class IBMMQ11Client extends AbstractJMS11Client implements JMSClient {
	
	public static final String PARAM_HOSTNAME = "hostName";
	public static final String PARAM_CHANNEL = "channel";
	public static final String PARAM_PORT = "port";
	public static final String PARAM_QUEUE_MANAGER = "queueManager";
	public static final String PARAM_TRANSPORT_TYPE = "transportType";
	
	private static final String CLASS_CONNECTION_FACTORY = "com.ibm.mq.jms.MQConnectionFactory";

	/**
	 * Creates new IBM MQ client instance.
	 * 
	 * @param clientID - Unique client ID
	 */
	public IBMMQ11Client(String clientID) {
		super(clientID);
	}

	@Override
	protected ConnectionFactory createConnectionFactory(Map<String, Object> parameters) throws Exception {
		Class<?> connectionFactoryClazz = ReflectionUtil.createClass(CLASS_CONNECTION_FACTORY);
		Object connectionFactoryInstance = ReflectionUtil.constructObject(connectionFactoryClazz);
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setHostName", 
				new Class[] {String.class}, new Object[] {parameters.get(PARAM_HOSTNAME)});
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setChannel", 
				new Class[] {String.class}, new Object[] {parameters.get(PARAM_CHANNEL)});
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setPort", 
				new Class[] {int.class}, new Object[] {parameters.get(PARAM_PORT)});
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setQueueManager", 
				new Class[] {String.class}, new Object[] {parameters.get(PARAM_QUEUE_MANAGER)});
		ReflectionUtil.executeMethod(connectionFactoryClazz, connectionFactoryInstance, "setTransportType", 
				new Class[] {int.class}, new Object[] {parameters.get(PARAM_TRANSPORT_TYPE)});
		return (ConnectionFactory) connectionFactoryInstance;
	}


}
