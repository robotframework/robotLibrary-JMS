package net.relaysoft.robot.jms.client;

import net.relaysoft.robot.jms.utils.StringUtils;

/**
 * Factory for creating JMS client wrappers. 
 * 
 * @author relaysoft.net
 *
 */
public final class JMSClientFactory {

	public static final int JMS_VERSION_1 = 1;
	public static final int JMS_VERSION_2 = 2;

	public static final String PROVIDER_JNDI = "JNDI";
	public static final String PROVIDER_ACTIVEMQ = "ACTIVEMQ";
	public static final String PROVIDER_IBMMQ = "IBMMQ";
	
	private JMSClientFactory(){}

	/**
	 * Create new JMS client wrapper. This client uses JMS version 1.1 implementation and is configured through JNDI.
	 * 
	 * @param clientID - Unique client ID
	 * @return New JMS client.
	 */
	public static JMSClient getJMSClient(String clientID){
		return getJMSClient(clientID, JMS_VERSION_1);		
	}

	/**
	 * Create new JMS client wrapper. This client uses given JMS version implementation and is configured through JNDI.
	 * 
	 * @param clientID - Unique client ID
	 * @param version - JMS implementation version to use (1 or 2)
	 * @return New JMS client.
	 */
	public static JMSClient getJMSClient(String clientID, int version){
		return getJMSClient(clientID, version, PROVIDER_JNDI);
	}

	/**
	 * Create new JMS client wrapper. This client uses given JMS version implementation and provider specific connection factory 
	 * configuration.
	 * 
	 * @param clientID - Unique client ID
	 * @param version - JMS implementation version to use (1 or 2)
	 * @param providerName - Name the MQ client provider e.g. <i>ACTIVEMQ</i> or <i>IBMMQ</i> 
	 * @return New JMS client.
	 */
	public static JMSClient getJMSClient(String clientID, int version, String providerName){
		JMSClient client = null;
		if(isJMSVersion2(version)){
			if(isDefaultClient(providerName)){
				client = new DefaultJMS20Client(clientID);
			} else if(providerName.equalsIgnoreCase(PROVIDER_IBMMQ)){
				client = new IBMMQ20Client(clientID);
			}
		} else {
			if(isDefaultClient(providerName)){
				client = new DefaultJMS11Client(clientID);
			} else if(providerName.equalsIgnoreCase(PROVIDER_ACTIVEMQ)){
				client = new ActiveMQ11Client(clientID);
			} else if(providerName.equalsIgnoreCase(PROVIDER_IBMMQ)){
				client = new IBMMQ11Client(clientID);
			}
		}
		if(client == null){
			throw new NullPointerException(getErrorMessage(providerName, version));
		}
		return client;
	}
	
	/**
	 * Construct error message for the situation where there no provider specific JMS client implementation available for the requested
	 * JMS version. 
	 * 
	 * @param providerName - Name the MQ client provider
	 * @return Error message.
	 */
	private static String getErrorMessage(String providerName, int version){
		String provider = (StringUtils.isNotBlank(providerName) ? providerName : "default");
		return "JMS version " + version + " implementation is not available for the " + provider + " provider.";
	}
	
	/**
	 * Checks whether given provider name matches to default provider.
	 * 
	 * @param providerName - Name the MQ client provider
	 * @return <code>true</code> if provider name is <code>null</code> or empty or equals {@link #PROVIDER_JNDI}
	 */
	private static boolean isDefaultClient(String providerName){
		return StringUtils.isBlank(providerName) || providerName.equals(PROVIDER_JNDI);
	}
	
	/**
	 * Check whether given JMS version is 2.0
	 * 
	 * @param version - JMS version
	 * @return <code>true</code> if given version is 2.0
	 */
	private static boolean isJMSVersion2(int version){
		return version == JMS_VERSION_2;
	}

}
