package net.relaysoft.robot.jms.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import net.relaysoft.robot.jms.exceptions.ConversionException;
import net.relaysoft.robot.jms.exceptions.JMSClientException;
import net.relaysoft.robot.jms.utils.MessageContentUtils;
import net.relaysoft.robot.jms.utils.MessageParameterNames;
import net.relaysoft.robot.jms.utils.MessageTypeEnum;
import net.relaysoft.robot.jms.utils.PrintUtil;
import net.relaysoft.robot.jms.utils.StringUtils;

/**
 * Abstract JMS API 1.1 version specific implementation of JMS client wrapper interface.
 * 
 * @author relaysoft.net
 *
 */
public abstract class AbstractJMS11Client extends AbstractJMSClient implements JMSClient {

	private static final int DEFAULT_BUFFER = 1024;

	private Connection connection = null;
	private Session session = null;
	private Map<String, TopicSubscriber> subscriptions = null;
	private Map<String, MessageConsumer> temporaryConsumers = null;

	/**
	 * Create new JMS client version 1.1 instance.
	 * 
	 * @param clientID - Unique client ID
	 */
	public AbstractJMS11Client(String clientID) {
		super(clientID);
		subscriptions = new HashMap<>();
		temporaryConsumers = new HashMap<>();
	}

	@Override
	public synchronized void closeConnection() throws JMSException {
		if(isConnected()){
			unsubscribeAllTopics();
			connection.close();
			temporaryConsumers.clear();
			super.closeConnection();
		}
	}

	@Override
	public String createTemporaryQueue() throws JMSException {
		TemporaryQueue queue = session.createTemporaryQueue();
		temporaryDestinations.put(queue.getQueueName(), queue);
		return queue.getQueueName();
	}

	@Override
	public String createTemporaryTopic() throws JMSException {
		TemporaryTopic topic = session.createTemporaryTopic();
		temporaryConsumers.put(topic.getTopicName(), createMessageConsumer(topic, null));
		temporaryDestinations.put(topic.getTopicName(), topic);
		return topic.getTopicName();
	}
	
	@Override
	public void deleteTemporaryQueue(String queueName) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			TemporaryQueue queue = (TemporaryQueue) temporaryDestinations.remove(queueName);
			if(temporaryConsumers.containsKey(queueName)){
				temporaryConsumers.remove(queueName).close();
			}
			queue.delete();
		}
	}
	
	@Override
	public void deleteTemporaryTopic(String queueName) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			TemporaryTopic topic = (TemporaryTopic) temporaryDestinations.remove(queueName);
			if(temporaryConsumers.containsKey(queueName)){
				temporaryConsumers.remove(queueName).close();
			}
			topic.delete();
		}
	}

	@Override
	public byte[] getMessageByteContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.BYTE)){
			return readBytes(message);
		} else if(type.equals(MessageTypeEnum.TEXT)){
			return readText(message).getBytes(StandardCharsets.UTF_8);
		} else if(type.equals(MessageTypeEnum.MAP)){
			return MessageContentUtils.convertMapToByteArray(readMap(message));    
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@Override
	public Map<String, Object> getMessageMapContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.MAP)){
			return readMap(message);
		} else if(type.equals(MessageTypeEnum.BYTE)){
			return MessageContentUtils.convertByteArrayToMap(readBytes(message));
		} else if(type.equals(MessageTypeEnum.TEXT)){
			throw new ConversionException("Cannot convert JMS text message content into map.");
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@Override
	public String getMessageCorrelationID(String messageID) throws JMSException {
		return getMessage(messageID).getJMSCorrelationID();
	}

	@Override
	public String getMessageDestinationName(String messageID) throws JMSException {
		Destination destination = getMessage(messageID).getJMSDestination();
		if(destination instanceof Queue){
			return ((Queue) destination).getQueueName();
		} else {
			return ((Topic) destination).getTopicName();
		}
	}

	@Override
	public long getMessageExpirationTime(String messageID) throws JMSException {
		return getMessage(messageID).getJMSExpiration();
	}

	@Override
	public String getMessageID(String messageID) throws JMSException {
		return getMessage(messageID).getJMSMessageID();
	}

	@Override
	public int getMessagePriority(String messageID) throws JMSException {
		return getMessage(messageID).getJMSPriority();
	}
	
	@Override
	public Object getMessageProperty(String propertyName, String messageID) throws JMSException {
		return getMessage(messageID).getObjectProperty(propertyName);
	}

	@Override
	public String getMessageReplyToQueue(String messageID) throws JMSException {
		Destination destination = getMessage(messageID).getJMSReplyTo();
		if(destination instanceof Queue){
			return ((Queue) destination).getQueueName();
		} else {
			return ((Topic) destination).getTopicName();
		}
	}

	@Override
	public String getMessageTextContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.TEXT)){
			return readText(message);			
		} else if(type.equals(MessageTypeEnum.BYTE)){
			return new String(readBytes(message), StandardCharsets.UTF_8);
		} else if(type.equals(MessageTypeEnum.MAP)){
			return readMap(message).toString();
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@Override
	public long getMessageTimestamp(String messageID) throws JMSException {
		return getMessage(messageID).getJMSTimestamp();
	}

	@Override
	public String getMessageType(String messageID) throws JMSException {
		return getMessage(messageID).getJMSType();
	}

	@Override
	public int getQueueDepth(String queueName) throws JMSException {
		int depth = 0;
		try(QueueBrowser browser = createBrowser(queueName, null)){
			Enumeration<?> e = browser.getEnumeration();
			while (e.hasMoreElements()) {
				e.nextElement();
				depth++;
			}
		}
		return depth;
	}

	@Override
	public void initializeClient(Map<String, Object> parameters) throws Exception {
		factory = createConnectionFactory(parameters);
	}

	@Override
	public synchronized void openConnection() throws JMSException {
		openConnection(null, null);
	}

	@Override
	public synchronized void openConnection(String userName, String password) throws JMSException {
		if(!isConnected()){
			connection = createConnection(getClientID(), userName, password);
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			connection.start();
			super.openConnection();
		}
	}

	@Override
	public String peekFromQueue(String queueName, String messageSelector) throws JMSException {
		try(QueueBrowser browser = createBrowser(queueName, messageSelector)){
			Enumeration<?> e = browser.getEnumeration();
			while (e.hasMoreElements()) {
				Message message = (Message) e.nextElement();
				addMessage(message);
				return message.getJMSMessageID();
			}
		}
		return null;
	}

	@Override
	public void publishIntoTopic(String topicName, String content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException {
		publishIntoTopic(topicName, createMessage(content, parameters, properties), parameters);
	}

	@Override
	public void publishIntoTopic(String topicName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException {
		publishIntoTopic(topicName, createMessage(content, parameters, properties), parameters);
	}

	@Override
	public void publishIntoTopic(String topicName, Map<String, Object> content, Map<String, Object> parameters,
			Map<String, Object> properties) throws JMSException {
		publishIntoTopic(topicName, createMessage(content, parameters, properties), parameters);
	}

	@Override
	public void purgeQueue(String queueName) throws JMSException {
		int queueDepth = getQueueDepth(queueName);
		for(int i = 0 ; i < queueDepth ; i++){
			try(MessageConsumer consumer = session.createConsumer(createQueue(queueName))){
				consumer.receive(1000);
			}
		}
	}

	@Override
	public String readFromQueue(String queueName, long timeout, String messageSelector) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			try(MessageConsumer consumer = createMessageConsumer((Queue) temporaryDestinations.get(queueName), messageSelector)){
				return addMessage(consumer.receive(timeout));
			} 	
		} else {
			try(MessageConsumer consumer = createMessageConsumer(createQueue(queueName), messageSelector)){
				return addMessage(consumer.receive(timeout));
			}
		}
	}

	@Override
	public String readFromTopic(String name, long timeout) throws JMSException {
		if(subscriptions.containsKey(name)){
			return addMessage(subscriptions.get(name).receive(timeout));
		} else if(temporaryDestinations.containsKey(name)) {
			return addMessage(temporaryConsumers.get(name).receive(timeout));
		} else {
			throw new JMSException("No active subscription or temporary topic with name " + name);
		}
	}

	@Override
	public void subscribeTopic(String topicName, String name, String messageSelector) throws JMSException {
		subscriptions.put(name, session.createDurableSubscriber(createTopic(topicName), name, messageSelector, false));		
	}

	@Override
	public void unsubscribeTopic(String name) throws JMSException {
		if(subscriptions.containsKey(name)){
			TopicSubscriber subscriber = subscriptions.remove(name);
			subscriber.close();
			session.unsubscribe(name);
		}
	}

	@Override
	public void writeIntoQueue(String queueName, String content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException {
		writeIntoQueue(queueName, createMessage(content, parameters, properties), parameters);
	}

	@Override
	public void writeIntoQueue(String queueName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException {
		writeIntoQueue(queueName, createMessage(content, parameters, properties), parameters);
	}

	@Override
	public void writeIntoQueue(String queueName, Map<String, Object> content, Map<String, Object> parameters, Map<String, Object> properties) 
			throws JMSException {
		writeIntoQueue(queueName, createMessage(content, parameters, properties), parameters);
	}

	/**
	 * Convert object into {@link Long}.
	 * 
	 * @param value - Object value to convert. Valid objects are {@link String}, {@link Integer} and {@link Long}
	 * @return Object as long value.
	 */
	private long convertValueToLong(Object value){
		if(value instanceof Integer){
			return Long.valueOf((int) value);
		} else if(value instanceof String){
			return Long.parseLong((String) value);
		} else {
			return (long) value;
		}
	}

	/**
	 * Create new queue browser.
	 * 
	 * @param queueName - Name of the queue to browse
	 * @param messageSelector - Optional message selector for the browser
	 * @return Queue browser instance
	 * @throws JMSException If creation of browser fails for any reason.
	 */
	private QueueBrowser createBrowser(String queueName, String messageSelector) throws JMSException {
		return session.createBrowser(createQueue(queueName), messageSelector);
	}

	/**
	 * Create new bytes JMS message.
	 * 
	 * @param is - Message content
	 * @return New JMS bytes message.
	 * @throws JMSException If creation of bytes message fails for any reason.
	 */
	private Message createBytesMessage(InputStream is) throws JMSException {
		byte[] buffer = new byte[DEFAULT_BUFFER];
		BytesMessage message = session.createBytesMessage();
		int bytes = 0;
		try {
			while ((bytes = is.read(buffer)) > -1) {
				message.writeBytes(buffer, 0, bytes);
			}
		} catch (IOException e) {
			PrintUtil.printOut("Failed to read bytes from inputstream. " + e.getMessage());
			throw new JMSException("Failed to create JMS bytes message.");
		}	
		return message;
	}

	/**
	 * Create new JMS connection.
	 * 
	 * @param clientID - Client ID for the connection
	 * @param userName - Optional caller user name
	 * @param password - Optional caller password
	 * @return New JMS connection instance.
	 * @throws JMSException If connection creation fails for any reason.
	 */
	private Connection createConnection(String clientID, String userName, String password) throws JMSException {
		Connection newConnection = factory.createConnection(userName, password);
		newConnection.setClientID(clientID);
		return newConnection;
	}

	/**
	 * Create new message consumer.
	 * 
	 * @param destination - Name of the target queue or topic for the consumer
	 * @param messageSelector - Optional message selector string for filtering messages
	 * @return New message consumer for the session.
	 * @throws JMSException If creating message consumer fails for any reason.
	 */
	private MessageConsumer createMessageConsumer(Destination destination, String messageSelector) throws JMSException{
		return session.createConsumer(destination, messageSelector);	
	}

	/**
	 * Create new Map JMS message.
	 * 
	 * @param map - Message content
	 * @return New JMS Map message.
	 * @throws JMSException If creation of map message fails for any reason.
	 */
	private MapMessage createMapMessage(Map<String, Object> map) throws JMSException {
		MapMessage message = session.createMapMessage();
		map.entrySet().stream().forEach(entry -> {
			try {
				message.setObject(entry.getKey(), entry.getValue());
			} catch (JMSException e) {
				PrintUtil.printOut("Failed to set new object " + entry.getKey() + " into JMS map message. " + e.getMessage());
				throw new JMSClientException("Failed to create JMS map message.", e);
			}
		});
		return message;
	}

	/**
	 * Create new JMS message. Message type is based on given content e.g. JMS text message is created from string content and
	 * bytes message from byte array.
	 * <p>
	 * Additionally JMS message parameters and properties can be given for the message. 
	 * 
	 * @param content - Message content. Valid contents are {@link String}, {@link byte[]}, and {@link Map}
	 * @param headers - Optional JMS message parameters to set for the message
	 * @param properties - Optional JMS message properties to set for the message
	 * @return JMS message based on given content.
	 * @throws JMSException If JMS message creation fails.
	 */
	private Message createMessage(Object content, Map<String, Object> parameters, Map<String, Object> properties) throws JMSException {
		Message message = createMessage(content);
		setProperties(message, properties);
		setMessageParameters(message, parameters);
		return message;
	}

	/**
	 * Create new JMS message. Message type is based on given content e.g. JMS text message is created from string content and
	 * bytes message from byte array.
	 * 
	 * @param content - Message content. Valid contents are {@link String}, {@link byte[]}, and {@link Map}
	 * @return JMS message based on given content.
	 * @throws JMSException If JMS message creation fails.
	 */
	@SuppressWarnings("unchecked")
	private Message createMessage(Object content) throws JMSException {
		if(content instanceof String){
			return session.createTextMessage((String) content);
		} else if(content instanceof byte[]){
			try(InputStream is = new ByteArrayInputStream((byte[]) content)){
				return createBytesMessage(is);
			} catch (IOException e) {
				PrintUtil.printOut("Failed to automatically close inputstream after reading. " + e.getMessage());
			}
		} else if(content instanceof Map){
			return createMapMessage((Map<String, Object>) content);
		}
		return session.createTextMessage(null);
	}

	/**
	 * Create new message producer.
	 * 
	 * @param destination - Destination queue/topic name for the producer
	 * @return JMS message producer for the given queue/topic
	 * @throws JMSException If producer creation fails.
	 */
	private MessageProducer createProducer(Destination destination) throws JMSException{
		return session.createProducer(destination);
	}

	/**
	 * Create JMS queue instance
	 * 
	 * @param queueName - Name of the queue
	 * @return New JMS queue instance.
	 * @throws JMSException If queue instance creation fails.
	 */
	private Queue createQueue(String queueName) throws JMSException {
		return session.createQueue(queueName);
	}

	/**
	 * Create JMS topic instance.
	 * 
	 * @param topicName - Name of the topic
	 * @return New JMS topic instance.
	 * @throws JMSException If topic instance creation fails.
	 */
	private Topic createTopic(String topicName) throws JMSException {
		return session.createTopic(topicName);
	}

	/**
	 * Publish new message into JMS topic.
	 * 
	 * @param topicName - Name of the topic to publish
	 * @param message - JMS message to publish
	 * @param parameters - Parameters for the producer used for publishing
	 * @throws JMSException If publish fails.
	 */
	private void publishIntoTopic(String topicName, Message message, Map<String, Object> parameters) throws JMSException{
		MessageProducer producer = null;
		try{
			producer = createProducer(createTopic(topicName));
			setProducerParameters(producer, parameters);
			producer.send(message);
		} finally {
			if(producer != null){
				producer.close();
			}
		}
	}

	/**
	 * Read content from JMS bytes message.
	 * 
	 * @param message - JMS bytes message
	 * @return JMS message content.
	 * @throws JMSException If reading of bytes message fails.
	 */
	private byte[] readBytes(Message message) throws JMSException {
		BytesMessage bytesMessage = (BytesMessage) message;
		byte[] bytes = new byte[DEFAULT_BUFFER];
		byte[] content = null;
		try(ByteArrayOutputStream os = new ByteArrayOutputStream()){
			int c = 0;
			while ((c = bytesMessage.readBytes(bytes, DEFAULT_BUFFER)) > 0) {
				os.write(bytes, 0, c);
			}
			content = os.toByteArray();
		} catch (IOException e) {
			PrintUtil.printOut("Failed to close output stream. " + e.getMessage());
		} finally {
			bytesMessage.reset();
		}
		return content;
	}

	/**
	 * Read content from JMS map message.
	 * 
	 * @param message - JMS map message
	 * @return JMS message content.
	 * @throws JMSException If getting map object keys or values fails.
	 */
	private Map<String, Object> readMap(Message message) throws JMSException {
		Map<String, Object> map = new HashMap<>();
		MapMessage mapMessage = (MapMessage) message;
		@SuppressWarnings("unchecked")
		Enumeration<String> names = mapMessage.getMapNames();
		while(names.hasMoreElements()){
			String name = names.nextElement();
			Object value = mapMessage.getObject(name);
			map.put(name, value);
		}
		return map;
	}

	/**
	 * Read string content from JMS message.
	 * 
	 * @param message - JMS message
	 * @return JMS message content.
	 * @throws JMSException If reading content from message fails.
	 */
	private String readText(Message message) throws JMSException {
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.BYTE)){
			return new String(readBytes(message), StandardCharsets.UTF_8);
		} else if(type.equals(MessageTypeEnum.MAP)){
			return readMap(message).toString();
		} else {
			return ((TextMessage) message).getText();
		}
	}

	/**
	 * Set JMS message parameter.
	 * 
	 * @param message - JMS message in which parameter is set
	 * @param type - JMS message parameter type. Valid values are <code>replyTo</code>, <code>correlationId</code>, <code>type</code> 
	 * @param value - Parameter value
	 * @throws JMSException If setting parameter into message fails.
	 */
	private void setMessageParameter(Message message, String type, Object value) throws JMSException{
		if(StringUtils.isNotBlank(type) && value != null){
			if(type.equals(MessageParameterNames.REPLY_TO)){
				message.setJMSReplyTo(createQueue((String) value));
			} else if(type.equals(MessageParameterNames.CORRELATION_ID)){
				message.setJMSCorrelationID((String) value);
			} else if(type.equals(MessageParameterNames.TYPE)){
				message.setJMSType((String) value);
			}
		}
	}

	/**
	 * Set JMS message parameters from map.
	 * 
	 * @param message - JMS message in which parameters are set
	 * @param parameters - Message parameters in map
	 */
	private void setMessageParameters(Message message, Map<String, Object> parameters){
		if(parameters != null){
			parameters.entrySet().stream().forEach(e -> {
				try {
					setMessageParameter(message, e.getKey(), e.getValue());
				} catch (JMSException e1) {
					PrintUtil.printOut("Failed to set parameter " + e.getKey() + " into JMS message. " + e1.getMessage());
					throw new JMSClientException("Failed to set message parameters", e1);
				}
			});
		}
	}

	/**
	 * Set override message parameter for the JMS producer.
	 * 
	 * @param producer - JMS producer to set message parameter
	 * @param type - JMS parameter type. Valid values are <code>priority</code>, <code>timeToLive</code>, <code>deliveryDetail</code>,
	 * <code>deliveryMode</code> 
	 * @param value - parameter value
	 * @throws JMSException  If setting parameter into producer fails.
	 */
	private void setProducerParameter(MessageProducer producer, String type, Object value) throws JMSException{
		if(StringUtils.isNotBlank(type) && value != null){
			if(type.equals(MessageParameterNames.PRIORITY)){
				producer.setPriority((int) value);
			} else if(type.equals(MessageParameterNames.TIME_TO_LIVE)){
				producer.setTimeToLive(convertValueToLong(value));
			} else if(type.equals(MessageParameterNames.DELIVERY_DELAY)){
				producer.setDeliveryDelay(convertValueToLong(value));
			} else if(type.equals(MessageParameterNames.DELIVERY_MODE)){
				producer.setDeliveryMode((int) value);
			}
		}
	}

	/**
	 * Set override message parameters for the JMS producer from map.
	 * 
	 * @param producer - JMS producer to set message parameters
	 * @param parameters - Message parameters in map
	 */
	private void setProducerParameters(MessageProducer producer, Map<String, Object> parameters){
		if(parameters != null){
			parameters.entrySet().stream().forEach(e -> {
				try {
					setProducerParameter(producer, e.getKey(), e.getValue());
				} catch (JMSException e1) {
					PrintUtil.printOut("Failed to set parameter " + e.getKey() + " into JMS producer. " + e1.getMessage());
					throw new JMSClientException("Failed to set producer parameters.", e1);
				}
			});
		}
	}

	/**
	 * Set JMS message property.
	 * 
	 * @param message - JMS message in which property is set to
	 * @param name - Property name
	 * @param value - Property value
	 * @throws JMSException If setting message property fails.
	 */
	private void setProperty(Message message, String name, Object value) throws JMSException{
		if(StringUtils.isNotBlank(name) && value != null){
			message.setObjectProperty(name, value);
		}
	}

	/**
	 * Set JMS message properties from map. These will override default properties set for the message.
	 * 
	 * @param message - JMS message in which properties are set to
	 * @param properties - Properties to set into JMS message
	 */
	private void setProperties(Message message, Map<String, Object> properties){
		if(properties != null){
			properties.entrySet().stream().forEach(e -> {
				try {
					setProperty(message, e.getKey(), e.getValue());
				} catch (JMSException e1) {
					PrintUtil.printOut("Failed to set property " + e.getKey() + " into JMS message. " + e1.getMessage());
					throw new JMSClientException("Failed to set message properties.", e1);
				}
			});
		}
	}

	/**
	 * Remove subscription from all the topics which has active subscription.  
	 */
	private void unsubscribeAllTopics(){
		List<String> names = subscriptions.keySet().stream().collect(Collectors.toList());
		names.forEach(n -> {
			try {
				unsubscribeTopic(n);
			} catch (JMSException e) {
				PrintUtil.printOut("Failed to unsubscribe topic subscription " + n);
			}
		});
	}

	/**
	 * Writes JMS message into queue.
	 * 
	 * @param queueName - Queue name in which message should be written
	 * @param message - JMS message to write
	 * @param parameters - JMS parameters to use for producer when writing
	 * @throws JMSException In writing message into queue or producer creation fails.
	 */
	private void writeIntoQueue(String queueName, Message message, Map<String, Object> parameters) throws JMSException{		
		MessageProducer producer = null;
		try{
			producer = createProducer(createQueue(queueName));
			setProducerParameters(producer, parameters);
			producer.send(message);
		} finally {
			if(producer != null){
				producer.close();
			}
		}
	}

}
