package net.relaysoft.robot.jms.client;

import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.TextMessage;

import net.relaysoft.robot.jms.utils.MessageTypeEnum;
import net.relaysoft.robot.jms.utils.StringUtils;

/**
 * Abstract base implementation of JMS client wrapper interface.
 * 
 * @author relaysoft.net
 *
 */
public abstract class AbstractJMSClient implements JMSClient{

	protected static final String CONNECTION_FACTORY_NAME = "ConnectionFactory";
	protected static final String INVALID_MESSAGE_EXCEPTION = "Invalid JMS message type ";

	protected ConnectionFactory factory = null;
	protected Map<String, Destination> temporaryDestinations = null;

	private String clientID = null;
	private boolean connected = false;
	private Message latestMessage = null;
	private Map<String, Message> readMessages = null;

	/**
	 * Create new abstract JMS client instance.
	 * 
	 * @param clientID - Unique client ID
	 */
	public AbstractJMSClient(String clientID) {
		this.clientID = clientID;
		temporaryDestinations = new HashMap<>();
		readMessages = new HashMap<>();
	}

	/**
	 * Create new JMS provider specific connection factory instance. 
	 * 
	 * @param parameters - Parameters used for connection factory creation.
	 * @return New connection factory instance.
	 * @throws Exception If connection factory creation fails.
	 */
	protected abstract ConnectionFactory createConnectionFactory(Map<String, Object> parameters) throws Exception;

	@Override
	public void clearMessages() {
		readMessages.clear();
		latestMessage = null;
	}

	@Override
	public void closeConnection() throws JMSException {
		temporaryDestinations.clear();
		connected = false;	
	}

	@Override
	public String getClientID() {
		return clientID;
	}

	@Override
	public void openConnection() throws JMSException {
		connected = true;
	}

	@Override
	public MessageTypeEnum resolveMessageType(String messageID) {
		return resolveMessageType(getMessage(messageID));
	}

	/**
	 * Add JMS message instance into client's internal storage. 
	 * 
	 * @param message - Message to add
	 * @return Message's ID.
	 * @throws JMSException If resolving message's ID fails.
	 */
	protected String addMessage(Message message) throws JMSException{
		if(message != null){
			latestMessage = message;
			readMessages.put(message.getJMSMessageID(), message);
			return message.getJMSMessageID();
		}
		return null;
	}

	/**
	 * Get read message by this client instance.
	 * 
	 * @param messageID - JMS message ID. If <code>null</code> or given message ID is not read by this client, 
	 * latest read message is returned. 
	 * @return JMS message or <code>null</code> if client has not read any messages
	 */
	protected Message getMessage(String messageID){
		if(StringUtils.isNotBlank(messageID)){
			return readMessages.get(messageID);
		}
		return latestMessage;
	}

	/**
	 * @return <code>true</code> if client wrapper is connected to MQ provider.
	 */
	protected boolean isConnected(){
		return connected;
	}

	/**
	 * Resolves JMS message's type. 
	 * 
	 * @param message - Message to check
	 * @return {@link net.relaysoft.robot.jms.utils.MessageTypeEnum#TEXT TEXT} for text message, 
	 * {@link net.relaysoft.robot.jms.utils.MessageTypeEnum#BYTE BYTE} for bytes message and 
	 * {@link net.relaysoft.robot.jms.utils.MessageTypeEnum#MAP MAP} for map message. In any other case return <code>null</code>.
	 */
	protected MessageTypeEnum resolveMessageType(Message message) {
		if(message != null){
			if(message instanceof TextMessage){
				return MessageTypeEnum.TEXT;
			} else if(message instanceof BytesMessage){
				return MessageTypeEnum.BYTE;
			} else if(message instanceof MapMessage){
				return MessageTypeEnum.MAP;
			}
		}
		return null;
	}

}
