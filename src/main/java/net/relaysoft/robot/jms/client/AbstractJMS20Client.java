package net.relaysoft.robot.jms.client;

import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.Topic;

import net.relaysoft.robot.jms.exceptions.ConversionException;
import net.relaysoft.robot.jms.utils.MessageContentUtils;
import net.relaysoft.robot.jms.utils.MessageParameterNames;
import net.relaysoft.robot.jms.utils.MessageTypeEnum;
import net.relaysoft.robot.jms.utils.StringUtils;

/**
 * Abstract JMS API 2.x version specific implementation of JMS client wrapper interface.
 * 
 * @author relaysoft.net
 *
 */
public abstract class AbstractJMS20Client extends AbstractJMSClient implements JMSClient {

	private JMSContext context = null;
	private Map<String, JMSConsumer> subscriptions = null;
	protected Map<String, JMSConsumer> temporaryConsumers = null;

	/**
	 * Create new JMS client version 2.0 instance.
	 * 
	 * @param clientID - Unique client ID
	 */
	public AbstractJMS20Client(String clientID) {
		super(clientID);
		subscriptions = new HashMap<>();
		temporaryConsumers = new HashMap<>();
	}

	@Override
	public synchronized void closeConnection() throws JMSException {
		if(isConnected()){
			unsubscribeAllTopics();
			context.close();
			temporaryConsumers.clear();
			super.closeConnection();
		}
	}

	@Override
	public String createTemporaryQueue() throws JMSException {
		TemporaryQueue queue = context.createTemporaryQueue();
		temporaryDestinations.put(queue.getQueueName(), queue);
		return queue.getQueueName();
	}

	@Override
	public String createTemporaryTopic() throws JMSException {
		TemporaryTopic topic = context.createTemporaryTopic();
		temporaryConsumers.put(topic.getTopicName(), context.createConsumer(topic));
		temporaryDestinations.put(topic.getTopicName(), topic);
		return topic.getTopicName();
	}
	
	@Override
	public void deleteTemporaryQueue(String queueName) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			TemporaryQueue queue = (TemporaryQueue) temporaryDestinations.remove(queueName);
			if(temporaryConsumers.containsKey(queueName)){
				temporaryConsumers.remove(queueName).close();
			}
			queue.delete();
		}
	}
	
	@Override
	public void deleteTemporaryTopic(String queueName) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			TemporaryTopic topic = (TemporaryTopic) temporaryDestinations.remove(queueName);
			if(temporaryConsumers.containsKey(queueName)){
				temporaryConsumers.remove(queueName).close();
			}
			topic.delete();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public byte[] getMessageByteContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.BYTE)){
			return message.getBody(byte[].class);
		} else if(type.equals(MessageTypeEnum.TEXT)){
			return message.getBody(String.class).getBytes(StandardCharsets.UTF_8);
		} else if(type.equals(MessageTypeEnum.MAP)){
			MessageContentUtils.convertMapToByteArray(message.getBody(Map.class));
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getMessageMapContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.MAP)){
			return message.getBody(Map.class);
		} else if(type.equals(MessageTypeEnum.BYTE)){
			return MessageContentUtils.convertByteArrayToMap(message.getBody(byte[].class));
		} else if(type.equals(MessageTypeEnum.TEXT)){
			throw new ConversionException("Cannot convert JMS text message content into map.");
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@Override
	public String getMessageCorrelationID(String messageID) throws JMSException {
		return getMessage(messageID).getJMSCorrelationID();
	}

	@Override
	public String getMessageDestinationName(String messageID) throws JMSException {
		Destination destination = getMessage(messageID).getJMSDestination();
		if(destination instanceof Queue){
			return ((Queue) destination).getQueueName();
		} else {
			return ((Topic) destination).getTopicName();
		}
	}

	@Override
	public long getMessageExpirationTime(String messageID) throws JMSException {
		return getMessage(messageID).getJMSExpiration();
	}

	@Override
	public String getMessageID(String messageID) throws JMSException {
		return getMessage(messageID).getJMSMessageID();
	}

	@Override
	public int getMessagePriority(String messageID) throws JMSException {
		return getMessage(messageID).getJMSPriority();
	}
	
	@Override
	public Object getMessageProperty(String propertyName, String messageID) throws JMSException {
		return getMessage(messageID).getObjectProperty(propertyName);
	}

	@Override
	public String getMessageReplyToQueue(String messageID) throws JMSException {
		Destination destination = getMessage(messageID).getJMSReplyTo();
		if(destination instanceof Queue){
			return ((Queue) destination).getQueueName();
		} else {
			return ((Topic) destination).getTopicName();
		}
	}
	
	@Override
	public String getMessageTextContent(String messageID) throws JMSException {
		Message message = getMessage(messageID);
		MessageTypeEnum type = resolveMessageType(message);
		if(type.equals(MessageTypeEnum.TEXT)){
			return message.getBody(String.class);
		} else if(type.equals(MessageTypeEnum.BYTE)){
			return new String(message.getBody(byte[].class), StandardCharsets.UTF_8);
		} else if(type.equals(MessageTypeEnum.MAP)){
			message.getBody(Map.class).toString();
		}
		throw new IllegalArgumentException(INVALID_MESSAGE_EXCEPTION.concat(message.getClass().getName()));
	}

	@Override
	public long getMessageTimestamp(String messageID) throws JMSException {
		return getMessage(messageID).getJMSTimestamp();
	}

	@Override
	public String getMessageType(String messageID) throws JMSException {
		return getMessage(messageID).getJMSType();
	}

	@Override
	public int getQueueDepth(String queueName) throws JMSException {
		int depth = 0;
		try(QueueBrowser browser = context.createBrowser(createQueue(queueName))){			
			Enumeration<?> e = browser.getEnumeration();
			while (e.hasMoreElements()) {
				e.nextElement();
				depth++;
			}
		}
		return depth;
	}

	@Override
	public void initializeClient(Map<String, Object> parameters) throws Exception{
		factory = createConnectionFactory(parameters);
	}
	
	@Override
	public synchronized void openConnection() throws JMSException {
		openConnection(null, null);
	}

	@Override
	public synchronized void openConnection(String userName, String password) throws JMSException {
		if(!isConnected()){
			context = createContext(getClientID(), userName, password);
			context.start();
			super.openConnection();
		}		
	}
	
	@Override
	public String peekFromQueue(String queueName, String messageSelector) throws JMSException {
		try(QueueBrowser browser = context.createBrowser(createQueue(queueName), messageSelector)){
			Enumeration<?> e = browser.getEnumeration();
			while (e.hasMoreElements()) {
				Message message = (Message) e.nextElement();
				addMessage(message);
				return message.getJMSMessageID();
			}
		}
		return null;
	}

	@Override
	public void publishIntoTopic(String topicName, String content, Map<String, Object> parameters, Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createTopic(topicName), content);		
	}

	@Override
	public void publishIntoTopic(String topicName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createTopic(topicName), content);	
	}

	@Override
	public void publishIntoTopic(String topicName, Map<String, Object> content, Map<String, Object> parameters,
			Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createTopic(topicName), content);
	}

	@Override
	public void purgeQueue(String queueName) throws JMSException {
		int queueDepth = getQueueDepth(queueName);
		for(int i = 0 ; i < queueDepth ; i++){
			try(JMSConsumer consumer = context.createConsumer(createQueue(queueName))){
				consumer.receive(1000);
			}
		}
	}

	@Override
	public String readFromQueue(String queueName, long timeout, String messageSelector) throws JMSException {
		if(temporaryDestinations.containsKey(queueName)){
			return addMessage(readFromQueue((Queue) temporaryDestinations.get(queueName), timeout, messageSelector));
		} else {
			return addMessage(readFromQueue(createQueue(queueName), timeout, messageSelector));
		}
	}

	@Override
	public String readFromTopic(String name, long timeout) throws JMSException {
		if(subscriptions.containsKey(name)){
			return addMessage(subscriptions.get(name).receive(timeout));
		} else if(temporaryDestinations.containsKey(name)){
			return addMessage(temporaryConsumers.get(name).receive(timeout));
		} else {
			throw new JMSException("No active subscription or temporary topic with name " + name);
		}
	}

	@Override
	public void subscribeTopic(String topicName, String name, String messageSelector) throws JMSException {
		subscriptions.put(name, context.createDurableConsumer(createTopic(topicName), name, messageSelector, false));
	}

	@Override
	public void unsubscribeTopic(String name) {
		if(subscriptions.containsKey(name)){
			JMSConsumer consumer = subscriptions.remove(name);
			consumer.close();
			context.unsubscribe(name);		
		}
	}

	@Override
	public void writeIntoQueue(String queueName, String content, Map<String, Object> parameters, Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createQueue(queueName), content);
	}

	@Override
	public void writeIntoQueue(String queueName, byte[] content, Map<String, Object> parameters, Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createQueue(queueName), content);		
	}

	@Override
	public void writeIntoQueue(String queueName, Map<String, Object> content, Map<String, Object> parameters,
			Map<String, Object> properties) {
		JMSProducer producer = createProducer(parameters, properties);
		producer.send(createQueue(queueName), content);	
	}

	/**
	 * Create new JMS context instance.
	 * 
	 * @param userName - Optional user name used for authentication
	 * @param password - Optional password used for authentication 
	 */
	private JMSContext createContext(String clientID, String userName, String password) {
		JMSContext newContext = factory.createContext(userName, password);
		newContext.setClientID(clientID);
		return newContext;
	}

	/**
	 * Create new JMS queue instance.
	 * 
	 * @param queueName - Queue name
	 * @return Queue instance.
	 */
	private Queue createQueue(String queueName){
		return context.createQueue(queueName);
	}

	/**
	 * Create new JMS Producer.
	 * 
	 * @param headers - JMS headers to override when writing messages into destination
	 * @param properties - JMS properties to override when writing messages into destination
	 * @return JMS producer instance.
	 */
	private JMSProducer createProducer(Map<String, Object> headers, Map<String, Object> properties){
		JMSProducer producer = context.createProducer();
		setProducerParameters(producer, headers);
		setProperties(producer, properties);
		return producer;
	}

	/**
	 * Create new JMS topic instance.
	 * 
	 * @param topicName - Topic name
	 * @return Topic instance.
	 */
	private Topic createTopic(String topicName){
		return context.createTopic(topicName);
	}

	/**
	 * Read JMS message from queue.
	 * 
	 * @param queue - Queue to read from
	 * @param timeout - Timeout in milliseconds to wait for message to read
	 * @param messageSelector - Optional JMS message selector used for read
	 * @return JMS Message or <code>null</code> if there was no message in queue.
	 */
	private Message readFromQueue(Queue queue, long timeout, String messageSelector){
		try(JMSConsumer consumer = context.createConsumer(queue, messageSelector)){
			return consumer.receive(timeout);
		}
	}
	
	/**
	 * Set override message parameters for the JMS producer.
	 * 
	 * @param producer - JMS producer
	 * @param type - Header type
	 * @param value - Header value
	 */
	private void setProducerParameter(JMSProducer producer, String type, Object value){
		if(StringUtils.isNotBlank(type) && value != null){
			if(type.equals(MessageParameterNames.PRIORITY)){
				producer.setPriority((int) value);
			} else if(type.equals(MessageParameterNames.TIME_TO_LIVE)){
				producer.setTimeToLive((long) value);
			} else if(type.equals(MessageParameterNames.DELIVERY_DELAY)){
				producer.setDeliveryDelay((long) value);
			} else if(type.equals(MessageParameterNames.DELIVERY_MODE)){
				producer.setDeliveryMode((int) value);
			} else if(type.equals(MessageParameterNames.CORRELATION_ID)){
				producer.setJMSCorrelationID((String) value);
			} else if(type.equals(MessageParameterNames.REPLY_TO)){
				producer.setJMSReplyTo((Destination) createQueue((String) value));
			} else if(type.equals(MessageParameterNames.TYPE)){
				producer.setJMSType((String) value);
			}
		}
	}

	/**
	 * Set JMS parameters from map into producer which will use values to overwrite corresponding ones for each message delivered by it.
	 * 
	 * @param producer - JMS producer
	 * @param parameters - JMS parameters to override for the delivered messages 
	 */
	private void setProducerParameters(JMSProducer producer, Map<String, Object> parameters){
		if(parameters != null){
			parameters.entrySet().stream().forEach(e -> setProducerParameter(producer, e.getKey(), e.getValue()));
		}
	}

	/**
	 * Set override message property for the JMS producer.
	 * 
	 * @param producer - JMS producer
	 * @param name - Property name
	 * @param value - Property value.
	 */
	private void setProperty(JMSProducer producer, String name, Object value){
		if(StringUtils.isNotBlank(name) && value != null){
			producer.setProperty(name, value);
		}
	}

	/**
	 * Set properties from map into producer which will use values to overwrite corresponding ones for each message delivered by it.
	 * 
	 * @param producer - JMS producer
	 * @param properties - Properties to override for the delivered messages 
	 */
	private void setProperties(JMSProducer producer, Map<String, Object> properties){
		if(properties != null){
			properties.entrySet().stream().forEach(e -> setProperty(producer, e.getKey(), e.getValue()));
		}
	}

	/**
	 * Remove subscription from all the topics which has active subscription.
	 */
	private void unsubscribeAllTopics(){
		subscriptions.keySet().stream().collect(Collectors.toList()).forEach(this::unsubscribeTopic);
	}

}
