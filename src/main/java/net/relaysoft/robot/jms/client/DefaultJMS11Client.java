package net.relaysoft.robot.jms.client;

import java.util.Hashtable;
import java.util.Map;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Default JMS version 1 client implementation. This client's connection factory is configured through JNDI parameters.
 * 
 * @author relaysoft.net
 *
 */
public class DefaultJMS11Client extends AbstractJMS11Client implements JMSClient {

	public DefaultJMS11Client(String clientID) {
		super(clientID);
	}

	@Override
	protected ConnectionFactory createConnectionFactory(Map<String, Object> parameters) throws Exception {
		return (ConnectionFactory) createContext(parameters).lookup(CONNECTION_FACTORY_NAME);
	}
	
	/**
	 * Creates new Java naming context from map.
	 * 
	 * @param parameters - Map of parameters for the context
	 * @return Naming context.
	 * @throws NamingException If creating the context fails.
	 */
	private Context createContext(Map<String, Object> parameters) throws NamingException{
		Hashtable<Object, Object> environment = new Hashtable<>();
		environment.putAll(parameters);
		return new InitialContext(environment);
	}

}
