*** Settings ***
resource        ../resources/common.robot
Suite Setup     Initialize Test Client

*** Test Cases ***
Connection
    [Documentation]  Testing connection opening and closing
	Open Connection
    Close Connection