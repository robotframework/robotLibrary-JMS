*** Settings ***
library         BuiltIn
library         String
library         OperatingSystem
resource        ../resources/common.robot
Suite Setup     Open Test Connection
Suite Teardown  Close Test Connection

*** Variables ***
${FILE_PATH}     src/test/robotframework/resources/test_content.txt
&{MAP_CONTENT}   test1=value1  test2=value2

*** Test Cases ***
Create Temporary Queue
    [Documentation]  Test creating the temporary queue
    [Tags]  Unit
    [Setup]  Purge Test Queue
    ${name} =  Create Temporary Queue
    Should Not Be Empty  ${name}
    Write Into Queue  queueName=${name}  content=${TEXT_CONTENT}
    Read From Queue  queueName=${name}
    ${content} =  Get Message Text Content
    Should Be Equal  ${content}  ${TEXT_CONTENT}

Get Queue Depth
    [Documentation]  Test getting the current queue depth
    [Tags]  Unit
    [Setup]  Purge Test Queue
    ${depth} =  Get Queue Depth  queueName=${QUEUE}
    Should Be Equal  ${depth}  ${0}
    Add Test Messages Into Queue
    ${depth} =  Get Queue Depth  queueName=${QUEUE}
    Should Be Equal  ${depth}  ${3}
    
Purge Queue
    [Documentation]  Test purging all messages from the queue
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Test Queue Depth  ${0}
    Add Test Messages Into Queue
    Purge Queue  queueName=${QUEUE}
    Sleep  1s  Wait for queue to be purged
    Test Queue Depth  ${0}
    
 Read Message From Queue
    [Documentation]  Test reading JMS message from queue
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}
    ${id} =  Read From Queue  queueName=${QUEUE}
    Should Not Be Empty  ${id}
    ${messageId} =  Get Message ID
    Should Be Equal  ${id}  ${messageId}
    ${content} =  Get Message Text Content  messageID=${id}
    Should Be Equal  ${content}  ${TEXT_CONTENT}

Write Text Message Into Queue
    [Documentation]  Test writing JMS text message into queue
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}
    ${id} =  Read From Queue  queueName=${QUEUE}
    ${content} =  Get Message Text Content  messageID=${id}
    Should Be Equal  ${content}  ${TEXT_CONTENT}
    
Write Map Message Into Queue
    [Documentation]  Test writing JMS map message into queue
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue  queueName=${QUEUE}  content=${MAP_CONTENT}
    ${id} =  Read From Queue  queueName=${QUEUE}
    ${content} =  Get Message Map Content  messageID=${id}
    Should Be Equal  ${content}  ${MAP_CONTENT}
    
Write Text Message Into Queue From File
    [Documentation]  Test writing JMS text message into queue with content from file
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue From File  queueName=${QUEUE}  filePath=${FILE_PATH}  type=text
    ${id} =  Read From Queue  queueName=${QUEUE}
    ${content} =  Get Message Text Content  messageID=${id}
    ${fileContent} =  Get File  ${FILE_PATH}
    Should Be Equal  ${content}  ${fileContent}
    
Write Byte Message Into Queue From File
    [Documentation]  Test writing JMS byte message into queue with content from file
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue From File  queueName=${QUEUE}  filePath=${FILE_PATH}  type=byte
    ${id} =  Read From Queue  queueName=${QUEUE}
    ${content} =  Get Message Text Content  messageID=${id}
    ${fileContent} =  Get File  ${FILE_PATH}
    Should Be Equal  ${content}  ${fileContent}
    