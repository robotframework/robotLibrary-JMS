*** Settings ***
library         BuiltIn
library         String
library         OperatingSystem
resource        ../resources/common.robot
Suite Setup     Open Test Connection
Suite Teardown  Close Test Connection

*** Variables ***
${FILE_PATH}     src/test/robotframework/resources/test_content.txt
&{MAP_CONTENT}   test1=value1  test2=value2

*** Test Cases ***
Create Temporary Topic
    [Documentation]  Test creating the temporary topic
    [Tags]  Unit
    ${topicName} =  Create Temporary Topic
    Should Not Be Empty  ${topicName}
    Publish Into Topic  topicName=${topicName}  content=${TEXT_CONTENT}
    ${id} =  Read From Topic  name=${topicName}
    Should Not Be Empty  ${id}
    ${messageId} =  Get Message ID
    Should Be Equal  ${id}  ${messageId}
    ${content} =  Get Message Text Content  messageID=${id}
    Should Be Equal  ${content}  ${TEXT_CONTENT}
    
Subscribe and Unsubscribe Topic
    [Documentation]  Test topic subscription creation and cancellation
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Unsubscribe Test Topic
    Subscribe Test Topic
    
Read Message From Topic
    [Documentation]  Test reading JMS message from topic
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Publish Into Topic  topicName=${TOPIC}  content=${TEXT_CONTENT}
    ${id} =  Read From Topic  name=${SUBSCRIPTION}
    Should Not Be Empty  ${id}
    ${messageId} =  Get Message ID
    Should Be Equal  ${id}  ${messageId}
    ${content} =  Get Message Text Content  messageID=${id}
    Should Be Equal  ${content}  ${TEXT_CONTENT}

Publish Text Message Into Topic
    [Documentation]  Test publishing JMS text message into topic
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Publish Into Topic  topicName=${TOPIC}  content=${TEXT_CONTENT}
    ${id} =  Read From Topic  name=${SUBSCRIPTION}
    ${content} =  Get Message Text Content  messageID=${id}
    Should Be Equal  ${content}  ${TEXT_CONTENT}
    
Publish Map Message Into Topic
    [Documentation]  Test publishing JMS map message into topic
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Publish Into Topic  topicName=${TOPIC}  content=${MAP_CONTENT}
    ${id} =  Read From Topic  name=${SUBSCRIPTION}
    ${content} =  Get Message Map Content  messageID=${id}
    Should Be Equal  ${content}  ${MAP_CONTENT}
    
Publish Text Message Into Topic From File
    [Documentation]  Test publishing JMS text message into topic with content from file
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Publish Into Topic From File  topicName=${TOPIC}  filePath=${FILE_PATH}  type=text
    ${id} =  Read From Topic  name=${SUBSCRIPTION}
    ${content} =  Get Message Text Content  messageID=${id}
    ${fileContent} =  Get File  ${FILE_PATH}
    Should Be Equal  ${content}  ${fileContent}
    
Publish Byte Message Into Topic From File
    [Documentation]  Test publishing JMS byte message into topic with content from file
    [Tags]  Unit
    [Setup]  Setup Topic Test
    Publish Into Topic From File  topicName=${TOPIC}  filePath=${FILE_PATH}  type=byte
    ${id} =  Read From Topic  name=${SUBSCRIPTION}
    ${content} =  Get Message Text Content  messageID=${id}
    ${fileContent} =  Get File  ${FILE_PATH}
    Should Be Equal  ${content}  ${fileContent}
    
*** Keywords ***
Setup Topic Test
    Unsubscribe Test Topic
    Subscribe Test Topic
    