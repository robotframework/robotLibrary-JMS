*** Settings ***
library         BuiltIn
library         String
library         OperatingSystem
resource        ../resources/common.robot
Suite Setup     Open Test Connection
Suite Teardown  Close Test Connection

*** Variables ***
&{PARAMETERS}   correlationId=1234  replyTo=REPLY_QUEUE  type=test  priority=${9}  timeToLive=${60}
&{PROPERTIES}   prop1=value1  prop2=${2}  prop3=${TRUE}
${TEMP_PATH}    target/tempResources
${TEST_FILE_PATH}     src/test/robotframework/resources/test_content.txt
${RESULT_FILE_PATH}  ${TEMP_PATH}/result_content.txt
&{MAP_CONTENT}   test1=value1  test2=value2

*** Test Cases ***
Test Message Parameters    
    [Documentation]  Test setting and getting message parameters
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}  parameters=${PARAMETERS}
    ${id} =  Read From Queue  queueName=${QUEUE}  timeout=${5000}
    Should Not Be Empty  ${id}
    ${correlationId} =  Get Message Correlation ID
    Should Be Equal  ${correlationId}  1234
    ${destinationName} =  Get Message Destination Name
    Should Be Equal  ${destinationName}  ${QUEUE}
    ${expirationTime} =  Get Message Expiration Time
    Should Be True	${expirationTime} > 0
    ${messageId} =  Get Message ID
    Should Be Equal  ${messageId}  ${Id}
    ${priority} =  Get Message Priority
    Should Be Equal  ${priority}  ${9}
    ${replyTo} =  Get Message Reply To Queue
    Should Be Equal  ${replyTo}  REPLY_QUEUE
    ${timestamp} =  Get Message Timestamp
    Should Be True	${timestamp} > 0
    ${messageType} =  Get Message Type
    Should Be Equal  ${messageType}  test
    
Test Message Properties
    [Documentation]  Test setting and getting message parameters
    [Tags]  Unit
    [Setup]  Purge Test Queue
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}  properties=${PROPERTIES}
    ${id} =  Read From Queue  queueName=${QUEUE}  timeout=${5000}
    Should Not Be Empty  ${id}
    ${prop1} =  Get Message Property  prop1
    Should Be Equal  ${prop1}  value1
    ${prop2} =  Get Message Property  prop2
    Should Be Equal  ${prop2}  ${2}
    ${prop3} =  Get Message Property  prop3
    Should Be Equal  ${prop3}  ${TRUE}

Test Message Content
    [Documentation]  Test setting and getting message content
    [Tags]  Unit
    [Setup]  Run Keywords  Purge Test Queue  AND  Create Directory  ${TEMP_PATH}
    [Teardown]  Empty Directory  ${TEMP_PATH}
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}
    Read From Queue  queueName=${QUEUE}  timeout=${5000}
    ${content} =  Get Message Text Content
    Should Be Equal  ${content}  ${TEXT_CONTENT}
    Write Into Queue  queueName=${QUEUE}  content=${MAP_CONTENT}
    Read From Queue  queueName=${QUEUE}
    ${content} =  Get Message Map Content
    Should Be Equal  ${content}  ${MAP_CONTENT}
    Write Into Queue From File  queueName=${QUEUE}  filePath=${TEST_FILE_PATH}  type=byte
    Read From Queue  queueName=${QUEUE}
    Get Message Byte Content  filePath=${RESULT_FILE_PATH}
    ${resultFileContent} =  Get File  ${RESULT_FILE_PATH}
    ${testFileContent} =  Get File  ${TEST_FILE_PATH}
    Should Be Equal  ${resultFileContent}  ${testFileContent}
    