*** Settings ***
resource        variables.robot
Library			JMS

*** Keywords ***
Add Test Messages Into Queue
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}
    Write Into Queue  queueName=${QUEUE}  content=${TEXT_CONTENT}

Close Test Connection
    Close Connection

Initialize Test Client
    Initialize Client  ${CLIENT_ID}  ${CONNECTION_PARAMETERS}
    
Open Test Connection
    Sleep  1  reason=Wait for ActiveMQ to warm up
    Initialize Test Client
    Open Connection

Purge Test Queue
    Purge Queue  queueName=${QUEUE}
    Sleep  1s  Wait for queue to be purged

Subscribe Test Topic
    Subscribe Topic  ${TOPIC}  ${SUBSCRIPTION}

Test Queue Depth
    [Arguments]  ${expectedDepth}
    ${depth} =  Get Queue Depth  queueName=${QUEUE}
    Should Be Equal  ${depth}  ${expectedDepth}
    
Unsubscribe Test Topic    
    Unsubscribe Topic  ${SUBSCRIPTION}
    

    