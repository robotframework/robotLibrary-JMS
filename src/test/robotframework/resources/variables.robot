*** Variables ***
${INITIAL_CONTEXT_FACTORY}  org.apache.activemq.jndi.ActiveMQInitialContextFactory
${PROVIDER_URL}        		tcp://localhost:61616
${CLIENT_ID}                test
${QUEUE}                    QUEUE.ROBOT.TEST
${TOPIC}                    TOPIC.ROBOT.TEST
&{CONNECTION_PARAMETERS}    java.naming.factory.initial=${INITIAL_CONTEXT_FACTORY}  java.naming.provider.url=${PROVIDER_URL}
${TEXT_CONTENT}             Test content
${SUBSCRIPTION}             testSubscription