import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JMSTest {
	
	JMS jsm;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		jsm = new JMS();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJMS() {
		assertEquals("TEST SUITE", JMS.ROBOT_LIBRARY_SCOPE);
		assertEquals("1.0.0", JMS.ROBOT_LIBRARY_VERSION);
	}

	@Test
	public void testGetKeywordDocumentationString() {
		String result = jsm.getKeywordDocumentation("__intro__");
		assertTrue(result.length() > 256);
	}

}
