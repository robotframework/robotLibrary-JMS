package net.relaysoft.robot.jms.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.relaysoft.robot.jms.utils.ReflectionUtil;

public class ReflectionUtilTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateClass() throws Exception {
		Class<?> result = ReflectionUtil.createClass(String.class.getName());
		assertEquals(result.getName(), String.class.getName());
	}

	@Test
	public void testConstructObject() throws Exception {
		Class<?> clazz = ReflectionUtil.createClass(String.class.getName());
		Object obj = ReflectionUtil.constructObject(clazz);
		assertNotNull(obj);
		assertTrue(obj instanceof String);
		assertTrue(((String) obj).isEmpty());
	}

	@Test
	public void testExecuteMethod() throws Exception {
		Class<?> clazz = ReflectionUtil.createClass(String.class.getName());
		Object obj = new String("test");
		Boolean result = (Boolean) ReflectionUtil.executeMethod(clazz, obj, "endsWith", new Class[] {String.class}, new Object[] {"st"});
		assertTrue(result);
	}

}
