package net.relaysoft.robot.jms.utils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.relaysoft.robot.jms.utils.MessageContentUtils;

public class MessageContentUtilsTest {
	
	Map<String, Object> map;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		map = new HashMap<>();
		map.put("key1", "value1");
		map.put("key2", 1);
		map.put("key3", true);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertByteArrayToMap() {
		byte[] array = MessageContentUtils.convertMapToByteArray(map);
		Map<String, Object> result = MessageContentUtils.convertByteArrayToMap(array);
		assertThat(result, is(map));
	}

	@Test
	public void testConvertMapToByteArray() {
		byte[] result = MessageContentUtils.convertMapToByteArray(map);
		assertThat(MessageContentUtils.convertByteArrayToMap(result), is(map));
	}

}
