package net.relaysoft.robot.jms.keywords;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.relaysoft.robot.jms.client.JMSClient;
import net.relaysoft.robot.jms.client.JMSClientFactory;
import net.relaysoft.robot.jms.keywords.JMSKeywords;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JMSClientFactory.class)
public class JMSKeywordsTest {

	private static final String TEST_JNDI_PATH = System.getProperty("user.dir").concat("/src/test/resources/net/relaysoft/robot/jms/keywords/testjndi.properties");
	private static final String TEST_DATA_PATH = System.getProperty("user.dir").concat("/target/testData");

	private static final String CLIENT1_ID = "client";
	private static final String MESSAGE_ID = "message";
	private static final String QUEUE = "queue";
	private static final String TOPIC = "topic";
	private static final String MESSAGE_SELECTOR = "selector";
	private static final String PROVIDER_NAME = "provider";
	private static final String USERNAME = "user";
	private static final String PASSWORD = "passwd";
	private static final String CONTENT = "content";
	private static final String SUBSCRIPTION = "subscription";
	private static final String PROPERTY_NAME = "property";

	private static final int VERSION = 1;

	private static final long TIMEOUT = 1000;

	JMSClient client;
	JMSKeywords keywords;
	Map<String, Object> parameters;
	Map<String, Object> properties;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Path path = Paths.get(TEST_DATA_PATH);
		Files.createDirectories(path);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Path path = Paths.get(TEST_DATA_PATH);
		Files.list(path).forEach(p -> {
			try {
				Files.delete(p);
			} catch (IOException e) {
			}
		});
		Files.delete(path);
	}

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		parameters = mock(Map.class);
		properties = mock(Map.class);
		client = mock(JMSClient.class);
		when(client.getClientID()).thenReturn(CLIENT1_ID);
		when(client.getMessageByteContent(anyString())).thenReturn(CONTENT.getBytes(StandardCharsets.UTF_8));
		PowerMockito.mockStatic(JMSClientFactory.class);
		when(JMSClientFactory.getJMSClient(CLIENT1_ID, VERSION, PROVIDER_NAME)).thenReturn(client);
		keywords = new JMSKeywords();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJMSKeywords() {
		JMSKeywords keywords = new JMSKeywords();
		assertNotNull(keywords);
	}

	@Test
	public void testClearMessages() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.clearMessages();
		verify(client, times(1)).clearMessages();
	}

	@Test
	public void testCloseConnection() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.closeConnection();
		verify(client, times(1)).closeConnection();
	}

	@Test
	public void testCreateTemporaryQueue() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.createTemporaryQueue();
		verify(client, times(1)).createTemporaryQueue();
	}

	@Test
	public void testCreateTemporaryTopic() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.createTemporaryTopic();
		verify(client, times(1)).createTemporaryTopic();
	}

	@Test
	public void testDeleteTemporaryQueue() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.deleteTemporaryQueue(QUEUE);
		verify(client, times(1)).deleteTemporaryQueue(QUEUE);
	}

	@Test
	public void testDeleteTemporaryTopic() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.deleteTemporaryTopic(TOPIC);
		verify(client, times(1)).deleteTemporaryTopic(TOPIC);
	}

	@Test
	public void testGetMessageByteContent_WithMessageIdAndFilePath() throws Exception {
		String filePath = TEST_DATA_PATH.concat("/byteContent");
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageByteContent(MESSAGE_ID, filePath);
		verify(client, times(1)).getMessageByteContent(MESSAGE_ID);
		assertTrue(Files.exists(Paths.get(filePath)));
		assertEquals(CONTENT, getFileContent(Paths.get(filePath)));
	}

	@Test
	public void testGetMessageByteContentString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageByteContent(MESSAGE_ID);
		verify(client, times(1)).getMessageByteContent(MESSAGE_ID);
	}

	@Test
	public void testGetMessageByteContent() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageByteContent();
		verify(client, times(1)).getMessageByteContent(null);
	}

	@Test
	public void testGetMessageMapContentString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageMapContent(MESSAGE_ID);
		verify(client, times(1)).getMessageMapContent(MESSAGE_ID);
	}

	@Test
	public void testGetMessageMapContent() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageMapContent();
		verify(client, times(1)).getMessageMapContent(null);
	}

	@Test
	public void testGetMessageCorrelationIDString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageCorrelationID(MESSAGE_ID);
		verify(client, times(1)).getMessageCorrelationID(MESSAGE_ID);
	}

	@Test
	public void testGetMessageCorrelationID() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageCorrelationID();
		verify(client, times(1)).getMessageCorrelationID(null);
	}

	@Test
	public void testGetMessageDestinationNameString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageDestinationName(MESSAGE_ID);
		verify(client, times(1)).getMessageDestinationName(MESSAGE_ID);
	}

	@Test
	public void testGetMessageDestinationName() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageDestinationName();
		verify(client, times(1)).getMessageDestinationName(null);
	}

	@Test
	public void testGetMessageExpirationTimeString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageExpirationTime(MESSAGE_ID);
		verify(client, times(1)).getMessageExpirationTime(MESSAGE_ID);
	}

	@Test
	public void testGetMessageExpirationTime() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageExpirationTime();
		verify(client, times(1)).getMessageExpirationTime(null);
	}

	@Test
	public void testGetMessageID() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageID();
		verify(client, times(1)).getMessageID(null);
	}

	@Test
	public void testGetMessagePriorityString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessagePriority(MESSAGE_ID);
		verify(client, times(1)).getMessagePriority(MESSAGE_ID);
	}

	@Test
	public void testGetMessagePriority() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessagePriority();
		verify(client, times(1)).getMessagePriority(null);
	}

	@Test
	public void testGetMessageProperty() throws Exception{
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageProperty(PROPERTY_NAME);
		verify(client, times(1)).getMessageProperty(PROPERTY_NAME, null);
	}

	@Test
	public void testGetMessageReplyToQueueString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageReplyToQueue(MESSAGE_ID);
		verify(client, times(1)).getMessageReplyToQueue(MESSAGE_ID);
	}

	@Test
	public void testGetMessageReplyToQueue() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageReplyToQueue();
		verify(client, times(1)).getMessageReplyToQueue(null);
	}

	@Test
	public void testGetMessageTimestampString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageTimestamp(MESSAGE_ID);
		verify(client, times(1)).getMessageTimestamp(MESSAGE_ID);
	}

	@Test
	public void testGetMessageTimestamp() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageTimestamp();
		verify(client, times(1)).getMessageTimestamp(null);
	}

	@Test
	public void testGetMessageTypeString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageType(MESSAGE_ID);
		verify(client, times(1)).getMessageType(MESSAGE_ID);
	}

	@Test
	public void testGetMessageType() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageType();
		verify(client, times(1)).getMessageType(null);
	}

	@Test
	public void testGetQueueDepth() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getQueueDepth(QUEUE);
		verify(client, times(1)).getQueueDepth(QUEUE);
	}

	@Test
	public void testGetMessageTextContentString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageTextContent(MESSAGE_ID);
		verify(client, times(1)).getMessageTextContent(MESSAGE_ID);
	}

	@Test
	public void testGetMessageTextContent() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.getMessageTextContent();
		verify(client, times(1)).getMessageTextContent(null);
	}

	@Test
	public void testInitializeClientStringMapOfStringObjectIntegerString() throws Exception {
		keywords.initializeClient(CLIENT1_ID, parameters, PROVIDER_NAME, VERSION);
		verify(client, times(1)).initializeClient(parameters);
		PowerMockito.verifyStatic();
		JMSClientFactory.getJMSClient(CLIENT1_ID, 1, PROVIDER_NAME);
	}

	@Test
	public void testInitializeClientStringMapOfStringObjectString() throws Exception {
		when(JMSClientFactory.getJMSClient(CLIENT1_ID, VERSION, PROVIDER_NAME)).thenReturn(client);
		keywords.initializeClient(CLIENT1_ID, parameters, PROVIDER_NAME);
		verify(client, times(1)).initializeClient(parameters);
		PowerMockito.verifyStatic();
		JMSClientFactory.getJMSClient(CLIENT1_ID, VERSION, PROVIDER_NAME);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testInitializeClientFromFileStringStringIntegerString() throws Exception {
		keywords.initializeClientFromFile(CLIENT1_ID, TEST_JNDI_PATH, PROVIDER_NAME, VERSION);
		verify(client, times(1)).initializeClient(any(Map.class));
		PowerMockito.verifyStatic();
		JMSClientFactory.getJMSClient(CLIENT1_ID, VERSION, PROVIDER_NAME);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testInitializeClientFromFileStringStringString() throws Exception {
		when(JMSClientFactory.getJMSClient(CLIENT1_ID, VERSION, PROVIDER_NAME)).thenReturn(client);
		keywords.initializeClientFromFile(CLIENT1_ID, TEST_JNDI_PATH, PROVIDER_NAME);
		verify(client, times(1)).initializeClient(any(Map.class));
		PowerMockito.verifyStatic();
		JMSClientFactory.getJMSClient(CLIENT1_ID, 1, PROVIDER_NAME);
	}

	@Test
	public void testOpenConnectionStringString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.openConnection(USERNAME, PASSWORD);
		verify(client, times(1)).openConnection(USERNAME, PASSWORD);
	}

	@Test
	public void testOpenConnection() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.openConnection();
		verify(client, times(1)).openConnection(null, null);
	}

	@Test
	public void testPublishIntoTopic_textDataWithParamsAndProperties() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopic(TOPIC, CONTENT, parameters, properties);
		verify(client, times(1)).publishIntoTopic(TOPIC, CONTENT, parameters, properties);
	}

	@Test
	public void testPublishIntoTopic_byteDataWithParamsAndProperties() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopic(TOPIC, CONTENT.getBytes(StandardCharsets.UTF_8), parameters, properties);
		verify(client, times(1)).publishIntoTopic(TOPIC, CONTENT.getBytes(StandardCharsets.UTF_8), parameters, properties);
	}

	@Test
	public void testPublishIntoTopic_byteData() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopic(TOPIC, CONTENT.getBytes(StandardCharsets.UTF_8));
		verify(client, times(1)).publishIntoTopic(TOPIC, CONTENT.getBytes(StandardCharsets.UTF_8), null, null);
	}

	@Test
	public void testPublishIntoTopic_textData() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopic(TOPIC, CONTENT);
		verify(client, times(1)).publishIntoTopic(TOPIC, CONTENT, null, null);
	}

	@Test
	public void testPublishIntoTopicFromFile_byteContentWithParamsAndProperties() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopicFromFile(TOPIC, TEST_JNDI_PATH, "BYTE", parameters, properties);
		verify(client, times(1)).publishIntoTopic(TOPIC, "test=test".getBytes(StandardCharsets.UTF_8), parameters, properties);
	}

	@Test
	public void testPublishIntoTopicFromFile_textContentWithParamsAndProperties() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopicFromFile(TOPIC, TEST_JNDI_PATH, "TEXT", parameters, properties);
		verify(client, times(1)).publishIntoTopic(TOPIC, "test=test", parameters, properties);
	}

	@Test
	public void testPublishIntoTopicFromFile_byteContent() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopicFromFile(TOPIC, TEST_JNDI_PATH, "BYTE");
		verify(client, times(1)).publishIntoTopic(TOPIC, "test=test".getBytes(StandardCharsets.UTF_8), null, null);
	}

	@Test
	public void testPublishIntoTopicFromFile_textContent() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.publishIntoTopicFromFile(TOPIC, TEST_JNDI_PATH, "TEXT");
		verify(client, times(1)).publishIntoTopic(TOPIC, "test=test", null, null);
	}

	@Test
	public void testPurgeQueueString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.purgeQueue(QUEUE);
		verify(client, times(1)).purgeQueue(QUEUE);
	}

	@Test
	public void testReadFromQueueStringLongString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.readFromQueue(QUEUE, TIMEOUT, MESSAGE_SELECTOR);
		verify(client, times(1)).readFromQueue(QUEUE, TIMEOUT, MESSAGE_SELECTOR);
	}

	@Test
	public void testReadFromQueueStringLong() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.readFromQueue(QUEUE, TIMEOUT);
		verify(client, times(1)).readFromQueue(QUEUE, TIMEOUT, null);
	}

	@Test
	public void testReadFromQueueString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.readFromQueue(QUEUE);
		verify(client, times(1)).readFromQueue(QUEUE, JMSKeywords.DEFAULT_TIMEOUT, null);
	}

	@Test
	public void testReadFromTopicStringLong() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.readFromTopic(TOPIC, TIMEOUT);
		verify(client, times(1)).readFromTopic(TOPIC, TIMEOUT);
	}

	@Test
	public void testReadFromTopicString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.readFromTopic(TOPIC);
		verify(client, times(1)).readFromTopic(TOPIC, JMSKeywords.DEFAULT_TIMEOUT);
	}

	@Test
	public void testSubscribeTopicStringStringString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.subscribeTopic(TOPIC, SUBSCRIPTION, MESSAGE_SELECTOR);
		verify(client, times(1)).subscribeTopic(TOPIC, SUBSCRIPTION, MESSAGE_SELECTOR);
	}

	@Test
	public void testSubscribeTopicStringString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.subscribeTopic(TOPIC, SUBSCRIPTION);
		verify(client, times(1)).subscribeTopic(TOPIC, SUBSCRIPTION, null);
	}

	@Test
	public void testUnsubscribeTopicString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.unsubscribeTopic(SUBSCRIPTION);
		verify(client, times(1)).unsubscribeTopic(SUBSCRIPTION);
	}

	@Test
	public void testWriteIntoQueueStringObjectMapOfStringObjectMapOfStringObject() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.writeIntoQueue(QUEUE, CONTENT, parameters, properties);
		verify(client, times(1)).writeIntoQueue(QUEUE, CONTENT, parameters, properties);
	}

	@Test
	public void testWriteIntoQueueStringObject() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.writeIntoQueue(QUEUE, CONTENT);
		verify(client, times(1)).writeIntoQueue(QUEUE, CONTENT, null, null);
	}

	@Test
	public void testWriteIntoQueueFromFileStringString() throws Exception {
		initializeMockClient(CLIENT1_ID);
		keywords.writeIntoQueueFromFile(QUEUE, TEST_JNDI_PATH);
		verify(client, times(1)).writeIntoQueue(QUEUE, "test=test".getBytes(StandardCharsets.UTF_8), null, null);
	}

	/**
	 * Return given file content as string.
	 * 
	 * @param filePath - Path to file
	 * @return Content as string
	 * @throws IOException If reading file content fails.
	 */
	private String getFileContent(Path filePath) throws IOException{
		StringBuffer buffer = new StringBuffer();
		try (BufferedReader r = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)) {
			r.lines().forEach(l -> buffer.append(l));
		}
		return buffer.toString();
	}

	/**
	 * Initialize new JMS client for testing
	 * 
	 * @param id - Client ID
	 * @throws Exception If anything fails.
	 */
	private void initializeMockClient(String id) throws Exception{
		keywords.initializeClient(id, parameters, PROVIDER_NAME, VERSION);
	}

}
