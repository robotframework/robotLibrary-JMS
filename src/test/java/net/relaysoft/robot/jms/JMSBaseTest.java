package net.relaysoft.robot.jms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicSubscriber;

import net.relaysoft.robot.jms.utils.MessageParameterNames;

/**
 * Abstract base test class for JMS client tests.
 * 
 * @author relaysoft.net
 *
 */
public abstract class JMSBaseTest {

	protected static final String MESSAGE_CORRELATIONID = "1111-2222-3333-4444";
	protected static final String MESSAGE_REPLY_TO = "replyQueue";
	protected static final String MESSAGE_TEXT_CONTENT = "test content";
	protected static final String MESSAGE_TYPE = "type";
	protected static final String MESSAGE_PROPERTY_KEY1 = "test1";
	protected static final String MESSAGE_PROPERTY_KEY2 = "test2";
	protected static final String MESSAGE_PROPERTY_VALUE1 = "value1";
	protected static final String QUEUE_NAME = "testQueue";
	protected static final String TOPIC_NAME = "testTopic";
	protected static final String SUBSCRIPTION_NAME = "subscription";
	protected static final String USERNAME = "username";
	protected static final String PASSWORD = "password";

	protected static final long MESSAGE_TIME_TO_LIVE = 60000;

	protected static final int MESSAGE_PRIORITY = 5;
	protected static final int MESSAGE_PROPERTY_VALUE2 = 2;

	private static final String CLIENT_ID = "testClientId";

	private static final int DEFAULT_BUFFER = 1024;

	Connection connection = null;
	Session session = null;
	TopicSubscriber subscription = null;

	public JMSBaseTest() {}

	/**
	 * Creates base test JMS parameters for the message.
	 * 
	 * @return Parameter map.
	 */
	protected Map<String, Object> createTestParameters(){
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(MessageParameterNames.CORRELATION_ID, MESSAGE_CORRELATIONID);
		parameters.put(MessageParameterNames.REPLY_TO, MESSAGE_REPLY_TO);
		parameters.put(MessageParameterNames.TYPE, MESSAGE_TYPE);
		parameters.put(MessageParameterNames.TIME_TO_LIVE, MESSAGE_TIME_TO_LIVE);
		parameters.put(MessageParameterNames.PRIORITY, MESSAGE_PRIORITY);
		return parameters;	
	}

	/**
	 * Creates base test JMS properties for the message.
	 * 
	 * @return Properties map.
	 */
	protected Map<String, Object> createTestProperties(){
		Map<String, Object> properties = new HashMap<>();
		properties.put(MESSAGE_PROPERTY_KEY1, MESSAGE_PROPERTY_VALUE1);
		properties.put(MESSAGE_PROPERTY_KEY2, MESSAGE_PROPERTY_VALUE2);
		return properties;
	}

	/**
	 * Create test queue with name {@link #QUEUE_NAME}.
	 * 
	 * @throws JMSException If queue creation fails.
	 */
	protected Queue createTestQueue() throws JMSException{
		return session.createQueue(QUEUE_NAME);
	}

	/**
	 * Create test topic with name {@link #TOPIC_NAME}.
	 * 
	 * @throws JMSException If topic creation fails.
	 */
	protected Topic createTestTopic() throws JMSException{
		return session.createTopic(TOPIC_NAME);
	}

	/**
	 * Creates test JMS message.
	 * 
	 * @param content - Content for the message
	 * @return Test JMS message.
	 * @throws JMSException If message creation fails.
	 * @throws IOException If reading byte content fails in case that given content was byte array.
	 */
	protected Message createTestMessage(Object content) throws JMSException, IOException{
		Message message = null;
		if(content instanceof String){
			message = session.createTextMessage();
			((TextMessage) message).setText((String) content);
		} else if(content instanceof byte[]){
			message = session.createBytesMessage();		
			int bytes = 0;
			InputStream is = new ByteArrayInputStream((byte[]) content);
			byte[] buffer = new byte[DEFAULT_BUFFER];
			while ((bytes = is.read(buffer)) > -1) {
				((BytesMessage) message).writeBytes(buffer, 0, bytes);
			}
		} else if(content instanceof Map){
			message = session.createMapMessage();
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) content;
			for(Map.Entry<String, Object> entry : map.entrySet()){
				((MapMessage) message).setObject(entry.getKey(), entry.getValue());
			}
		}
		if(message != null){
			message.setJMSCorrelationID(MESSAGE_CORRELATIONID);
			message.setJMSReplyTo(session.createQueue(MESSAGE_REPLY_TO));
			message.setJMSType(MESSAGE_TYPE);
			message.setJMSPriority(MESSAGE_PRIORITY);
			message.setJMSExpiration(MESSAGE_TIME_TO_LIVE);
		}
		return message;
	}

	protected void initializeTestConnection(ConnectionFactory factory) throws Exception{
		while(connection == null){
			try{
				connection = factory.createConnection();
				connection.setClientID(CLIENT_ID);	
				session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		connection.start();
	}

	protected void publishTestMessageIntoTopic(Object content) throws JMSException, IOException{
		session.createProducer(session.createTopic(TOPIC_NAME)).send(createTestMessage(content));
	}

	protected byte[] readBytes(Message message) throws JMSException, IOException {
		BytesMessage bytesMessage = (BytesMessage)message;
		byte[] bytes = new byte[DEFAULT_BUFFER];
		try(ByteArrayOutputStream os = new ByteArrayOutputStream()){
			int c = 0;
			while ((c = bytesMessage.readBytes(bytes, DEFAULT_BUFFER)) > 0) {
				os.write(bytes, 0, c);
			}
			return os.toByteArray();
		} finally {
			bytesMessage.reset();
		}
	}

	protected Map<String, Object> readMap(Message message) throws JMSException {
		Map<String, Object> map = new HashMap<>();
		MapMessage mapMessage = (MapMessage) message;
		@SuppressWarnings("unchecked")
		Enumeration<String> names = mapMessage.getMapNames();
		while(names.hasMoreElements()){
			String name = names.nextElement();
			Object value = mapMessage.getObject(name);
			map.put(name, value);
		}
		return map;
	}

	protected Message readMessageFromQueue() throws JMSException{
		try(MessageConsumer consumer = session.createConsumer(session.createQueue(QUEUE_NAME))){
			return consumer.receive();
		}	
	}

	protected Message readMessageFromTopic() throws JMSException{
		return subscription.receive();
	}

	protected void subscribeTestTopic() throws JMSException{
		subscription = session.createDurableSubscriber(session.createTopic(TOPIC_NAME), SUBSCRIPTION_NAME);
	}

	protected void writeTestMessageToQueue(Object content) throws JMSException, IOException{	
		MessageProducer producer = session.createProducer(session.createQueue(QUEUE_NAME));
		producer.setPriority(MESSAGE_PRIORITY);
		producer.setTimeToLive(MESSAGE_TIME_TO_LIVE);
		producer.send(createTestMessage(content));
	}

	protected void writeTestMessageToQueue(Message message) throws JMSException, IOException{	
		MessageProducer producer = session.createProducer(session.createQueue(QUEUE_NAME));
		producer.setPriority(message.getJMSPriority());
		producer.setTimeToLive(message.getJMSExpiration());
		producer.send(message);
	}

}
