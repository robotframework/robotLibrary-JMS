package net.relaysoft.robot.jms.client;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.awaitility.Awaitility.*;
import org.awaitility.Duration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.naming.Context;

import org.apache.activemq.jndi.ActiveMQInitialContextFactory;
import org.apache.activemq.junit.EmbeddedActiveMQBroker;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import net.relaysoft.robot.jms.JMSBaseTest;
import net.relaysoft.robot.jms.client.JMSClient;
import net.relaysoft.robot.jms.client.JMSClientFactory;
import net.relaysoft.robot.jms.utils.MessageTypeEnum;

public class JMS11ClientTest extends JMSBaseTest{

	private static final String CLIENT_ID = "test";

	private static final long TIMEOUT = 1000;

	@Rule
	public EmbeddedActiveMQBroker broker = new EmbeddedActiveMQBroker();

	private JMSClient client = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		client = JMSClientFactory.getJMSClient(CLIENT_ID);
		client.initializeClient(createDefaultParameters());		
		client.openConnection(USERNAME, PASSWORD);
		initializeTestConnection(broker.createConnectionFactory());
	}

	@After
	public void tearDown() throws Exception {
		client.closeConnection();
	}

	@Test
	public void testCloseConnection() throws JMSException {
		client.closeConnection();
	}

	@Test
	public void testOpenConnection() throws JMSException {
		client.openConnection();
	}

	@Test
	public void testGetClientId(){
		assertEquals(CLIENT_ID, client.getClientID());
	}

	@Test
	public void testGetMessageByteContent() throws JMSException, IOException {
		byte[] expectedContent = MESSAGE_TEXT_CONTENT.getBytes(StandardCharsets.UTF_8);
		writeTestMessageToQueue(expectedContent);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		byte[] content = client.getMessageByteContent(id);
		assertArrayEquals(expectedContent, content);
		content = client.getMessageByteContent(null);
		assertArrayEquals(expectedContent, content);
	}

	@Test
	public void testGetMessageMapContent() throws JMSException, IOException {
		Map<String, Object> expectedContent = new HashMap<>();
		expectedContent.put("test1", "value");
		expectedContent.put("test2", 1);
		writeTestMessageToQueue(expectedContent);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		Map<String, Object> content = client.getMessageMapContent(id);
		assertThat(content, is(expectedContent));
		content = client.getMessageMapContent(null);
		assertThat(content, is(expectedContent));
	}

	@Test
	public void testGetMessageCorrelationID() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_CORRELATIONID, client.getMessageCorrelationID(id));
		assertEquals(MESSAGE_CORRELATIONID, client.getMessageCorrelationID(null));
	}

	@Test
	public void testGetMessageDestinationName_queue() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(QUEUE_NAME, client.getMessageDestinationName(id));
		assertEquals(QUEUE_NAME, client.getMessageDestinationName(null));
	}

	@Test
	public void testGetMessageDestinationName_topic() throws JMSException, IOException {
		client.subscribeTopic(TOPIC_NAME, SUBSCRIPTION_NAME, null);
		publishTestMessageIntoTopic(MESSAGE_TEXT_CONTENT);
		String id = client.readFromTopic(SUBSCRIPTION_NAME, TIMEOUT);
		assertEquals(TOPIC_NAME, client.getMessageDestinationName(id));
		assertEquals(TOPIC_NAME, client.getMessageDestinationName(null));
	}

	@Test
	public void testGetMessageExpirationTime() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		Instant instant = Instant.ofEpochMilli(client.getMessageExpirationTime(null));
		assertTrue(instant.isAfter(Instant.now()) && instant.isBefore(Instant.now().plusMillis(MESSAGE_TIME_TO_LIVE)));
		instant = Instant.ofEpochMilli(client.getMessageExpirationTime(id));
		assertTrue(instant.isAfter(Instant.now()) && instant.isBefore(Instant.now().plusMillis(MESSAGE_TIME_TO_LIVE)));
	}

	@Test
	public void testGetMessageID() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(id, client.getMessageID(id));
		assertEquals(id, client.getMessageID(null));
	}

	@Test
	public void testGetMessagePriority() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_PRIORITY, client.getMessagePriority(id));
		assertEquals(MESSAGE_PRIORITY, client.getMessagePriority(null));
	}
	
	@Test
	public void testGetMessageProperty() throws JMSException{
		client.writeIntoQueue(QUEUE_NAME, MESSAGE_TEXT_CONTENT, createTestParameters(), createTestProperties());
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_PROPERTY_VALUE1, client.getMessageProperty(MESSAGE_PROPERTY_KEY1, id));
		assertEquals(MESSAGE_PROPERTY_VALUE2, client.getMessageProperty(MESSAGE_PROPERTY_KEY2, id));
	}

	@Test
	public void testGetMessageReplyToQueue() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_REPLY_TO, client.getMessageReplyToQueue(id));
		assertEquals(MESSAGE_REPLY_TO, client.getMessageReplyToQueue(null));
	}

	@Test
	public void testGetMessageReplyToQueue_topic() throws JMSException, IOException {
		Message message = createTestMessage(MESSAGE_TEXT_CONTENT);
		message.setJMSReplyTo(createTestTopic());
		writeTestMessageToQueue(message);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(TOPIC_NAME, client.getMessageReplyToQueue(id));
		assertEquals(TOPIC_NAME, client.getMessageReplyToQueue(null));
	}

	@Test
	public void testGetMessageTimestamp() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		Instant instant = Instant.ofEpochMilli(client.getMessageTimestamp(id));
		assertTrue(instant.isBefore(Instant.now()));
	}

	@Test
	public void testGetMessageType() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_TYPE, client.getMessageType(id));
		assertEquals(MESSAGE_TYPE, client.getMessageType(null));
	}

	@Test
	public void testGetQueueDepth() throws JMSException, IOException {
		assertEquals(0, client.getQueueDepth(QUEUE_NAME));
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		assertEquals(1, client.getQueueDepth(QUEUE_NAME));
	}

	@Test
	public void testGetMessageTextContent() throws JMSException, IOException {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MESSAGE_TEXT_CONTENT, client.getMessageTextContent(id));
		assertEquals(MESSAGE_TEXT_CONTENT, client.getMessageTextContent(null));
	}

	@Test
	public void testPublishIntoTopicStringStringMapOfStringObjectMapOfStringObject() throws JMSException {
		subscribeTestTopic();
		client.publishIntoTopic(TOPIC_NAME, MESSAGE_TEXT_CONTENT, new HashMap<>(), new HashMap<>());
		Message message = readMessageFromTopic();
		assertNotNull(message);
		assertEquals(MESSAGE_TEXT_CONTENT, ((TextMessage) message).getText());
	}

	@Test
	public void testPublishIntoTopicStringByteArrayMapOfStringObjectMapOfStringObject() throws JMSException, IOException {
		subscribeTestTopic();
		byte[] expectedContent = MESSAGE_TEXT_CONTENT.getBytes(StandardCharsets.UTF_8);
		client.publishIntoTopic(TOPIC_NAME, expectedContent, new HashMap<>(), new HashMap<>());
		Message message = readMessageFromTopic();
		assertArrayEquals(expectedContent, readBytes(message));
	}

	@Test
	public void testPublishIntoTopicStringMapOfStringObjectMapOfStringObjectMapOfStringObject() throws JMSException {
		subscribeTestTopic();
		Map<String, Object> expectedContent = new HashMap<>();
		expectedContent.put("test1", "value");
		expectedContent.put("test2", 1);
		client.publishIntoTopic(TOPIC_NAME, expectedContent, new HashMap<>(), new HashMap<>());
		Message message = readMessageFromTopic();
		assertThat(readMap(message), is(expectedContent));
	}

	@Test
	public void testPurgeQueue() throws Exception {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		await().atMost(Duration.FIVE_SECONDS).until(isTestQueueNotEmpty());
		assertEquals(3, getTestQueueDepth());
		client.purgeQueue(QUEUE_NAME);
		await().atMost(Duration.FIVE_SECONDS).until(isTestQueueEmpty());
		assertEquals(0, getTestQueueDepth());
	}

	@Test
	public void testReadFromQueue() throws Exception {
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		await().atMost(Duration.FIVE_SECONDS).until(isTestQueueNotEmpty());
		assertEquals(1, getTestQueueDepth());
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		await().atMost(Duration.FIVE_SECONDS).until(isTestQueueEmpty());
		assertEquals(0, getTestQueueDepth());
		assertNotNull(id);
		assertEquals(MESSAGE_TEXT_CONTENT, client.getMessageTextContent(null));
	}

	@Test
	public void testReadFromQueue_emptyQueue() throws Exception {
		assertNull(client.readFromQueue(QUEUE_NAME, TIMEOUT, null));
	}

	@Test
	public void testReadFromTopic() throws JMSException, IOException {
		client.subscribeTopic(TOPIC_NAME, SUBSCRIPTION_NAME, null);
		publishTestMessageIntoTopic(MESSAGE_TEXT_CONTENT);
		String id = client.readFromTopic(SUBSCRIPTION_NAME, TIMEOUT);
		assertNotNull(id);
		assertEquals(MESSAGE_TEXT_CONTENT, client.getMessageTextContent(null));
	}

	@Test
	public void testSubscribeTopic() throws JMSException, IOException {
		client.subscribeTopic(TOPIC_NAME, SUBSCRIPTION_NAME, null);
		publishTestMessageIntoTopic(MESSAGE_TEXT_CONTENT);
		String id = client.readFromTopic(SUBSCRIPTION_NAME, TIMEOUT);
		assertNotNull(id);
	}

	@Test(expected=JMSException.class)
	public void testUnsubscribeTopic() throws JMSException, IOException, InterruptedException {
		client.subscribeTopic(TOPIC_NAME, SUBSCRIPTION_NAME, null);
		publishTestMessageIntoTopic(MESSAGE_TEXT_CONTENT);
		client.unsubscribeTopic(SUBSCRIPTION_NAME);
		client.readFromTopic(SUBSCRIPTION_NAME, TIMEOUT);
	}

	@Test
	public void testWriteIntoQueueStringStringMapOfStringObjectMapOfStringObject() throws JMSException {
		client.writeIntoQueue(QUEUE_NAME, MESSAGE_TEXT_CONTENT, createTestParameters(), createTestProperties());
		Message message = readMessageFromQueue();
		assertNotNull(message);
		assertEquals(MESSAGE_TEXT_CONTENT, ((TextMessage) message).getText());
		assertEquals(MESSAGE_PRIORITY, message.getJMSPriority());
		assertEquals(MESSAGE_REPLY_TO, ((Queue) message.getJMSReplyTo()).getQueueName());
		assertEquals(MESSAGE_CORRELATIONID, message.getJMSCorrelationID());
		assertEquals(MESSAGE_TYPE, message.getJMSType());
		assertTrue(message.getJMSExpiration() > 0);
	}

	@Test
	public void testWriteIntoQueueStringByteArrayMapOfStringObjectMapOfStringObject() throws JMSException, IOException {
		byte[] expectedContent = MESSAGE_TEXT_CONTENT.getBytes(StandardCharsets.UTF_8);
		client.writeIntoQueue(QUEUE_NAME, expectedContent, createTestParameters(), createTestProperties());
		Message message = readMessageFromQueue();
		assertArrayEquals(MESSAGE_TEXT_CONTENT.getBytes(StandardCharsets.UTF_8), readBytes(message));
		assertEquals(MESSAGE_PRIORITY, message.getJMSPriority());
		assertEquals(MESSAGE_REPLY_TO, ((Queue) message.getJMSReplyTo()).getQueueName());
		assertEquals(MESSAGE_CORRELATIONID, message.getJMSCorrelationID());
		assertEquals(MESSAGE_TYPE, message.getJMSType());
		assertTrue(message.getJMSExpiration() > 0);
	}

	@Test
	public void testWriteIntoQueueStringMapOfStringObjectMapOfStringObjectMapOfStringObject() throws JMSException {
		Map<String, Object> expectedContent = new HashMap<>();
		expectedContent.put("test1", "value");
		expectedContent.put("test2", 1);
		client.writeIntoQueue(QUEUE_NAME, expectedContent, createTestParameters(), createTestProperties());
		Message message = readMessageFromQueue();
		assertThat(readMap(message), is(expectedContent));
		assertEquals(MESSAGE_PRIORITY, message.getJMSPriority());
		assertEquals(MESSAGE_REPLY_TO, ((Queue) message.getJMSReplyTo()).getQueueName());
		assertEquals(MESSAGE_CORRELATIONID, message.getJMSCorrelationID());
		assertEquals(MESSAGE_TYPE, message.getJMSType());
		assertTrue(message.getJMSExpiration() > 0);
	}

	@Test
	public void testCreateTemporaryQueue() throws Exception{
		String result = client.createTemporaryQueue();
		assertNotNull(result);
		assertEquals(-1, broker.getMessageCount(result));
	}

	@Test
	public void testCreateTemporaryTopic() throws Exception{
		String result = client.createTemporaryTopic();
		assertNotNull(result);
		assertEquals(-1, broker.getMessageCount(result));
	}
	
	@Test
	public void testDeleteTemporaryQueue() throws Exception{
		String result = client.createTemporaryQueue();
		client.deleteTemporaryQueue(result);
	}

	@Test
	public void testDeleteTemporaryTopic() throws Exception{
		String result = client.createTemporaryTopic();
		client.deleteTemporaryTopic(result);
	}

	@Test(expected=NullPointerException.class)
	public void testClearMessages() throws JMSException, IOException{
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		client.clearMessages();
		client.getMessageID(null);
	}

	@Test
	public void testResolveMessageType_text() throws JMSException, IOException{
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MessageTypeEnum.TEXT, client.resolveMessageType(id));
		assertEquals(MessageTypeEnum.TEXT, client.resolveMessageType(null));
	}

	@Test
	public void testResolveMessageType_byte() throws JMSException, IOException{
		writeTestMessageToQueue(MESSAGE_TEXT_CONTENT.getBytes(StandardCharsets.UTF_8));
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MessageTypeEnum.BYTE, client.resolveMessageType(id));
		assertEquals(MessageTypeEnum.BYTE, client.resolveMessageType(null));
	}

	@Test
	public void testResolveMessageType_map() throws JMSException, IOException{
		Map<String, Object> content = new HashMap<>();
		content.put("test1", "value");
		content.put("test2", 1);
		writeTestMessageToQueue(content);
		String id = client.readFromQueue(QUEUE_NAME, TIMEOUT, null);
		assertEquals(MessageTypeEnum.MAP, client.resolveMessageType(id));
		assertEquals(MessageTypeEnum.MAP, client.resolveMessageType(null));
	}

	/** 
	 * @return Default connection parameters
	 */
	private Map<String, Object> createDefaultParameters(){
		Map<String, Object> params = new HashMap<>();
		params.put(Context.INITIAL_CONTEXT_FACTORY, ActiveMQInitialContextFactory.class.getName());
		params.put(Context.PROVIDER_URL, getProviderUrl());
		return params;
	}

	/**
	 * @return Embedded test MQ broker URL.
	 */
	private String getProviderUrl(){
		return broker.getVmURL();
	}

	/**
	 * @return Test queue current depth.
	 * @throws Exception If getting message count fails.
	 */
	private int getTestQueueDepth() throws Exception{
		return broker.getMessageCount(QUEUE_NAME);
	}

	/**
	 * @return <code>true</code> if test queue is empty
	 */
	private Callable<Boolean> isTestQueueEmpty() {
		return new Callable<Boolean>() {
			public Boolean call() throws Exception {
				return broker.getMessageCount(QUEUE_NAME) == 0;
			}
		};
	}
	
	/**
	 * @return <code>true</code> if test queue is not empty
	 */
	private Callable<Boolean> isTestQueueNotEmpty() {
		return new Callable<Boolean>() {
			public Boolean call() throws Exception {
				return broker.getMessageCount(QUEUE_NAME) > 0;
			}
		};
	}

}
