package net.relaysoft.robot.jms.client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import net.relaysoft.robot.jms.client.ActiveMQ11Client;
import net.relaysoft.robot.jms.client.DefaultJMS11Client;
import net.relaysoft.robot.jms.client.DefaultJMS20Client;
import net.relaysoft.robot.jms.client.JMSClient;
import net.relaysoft.robot.jms.client.JMSClientFactory;

public class JMSClientFactoryTest {
	
	private static final String CLIENT_ID = "test";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetJMSClientString() {
		JMSClient client = JMSClientFactory.getJMSClient(CLIENT_ID);
		assertNotNull(client);
		assertTrue(client instanceof DefaultJMS11Client);
	}

	@Test
	public void testGetJMSClientStringInt_1() {
		JMSClient client = JMSClientFactory.getJMSClient(CLIENT_ID, JMSClientFactory.JMS_VERSION_1);
		assertNotNull(client);
		assertTrue(client instanceof DefaultJMS11Client);
	}
	
	@Test
	public void testGetJMSClientStringInt_2() {
		JMSClient client = JMSClientFactory.getJMSClient(CLIENT_ID, JMSClientFactory.JMS_VERSION_2);
		assertNotNull(client);
		assertTrue(client instanceof DefaultJMS20Client);
	}

	@Test
	public void testGetJMSClientStringIntString() {
		JMSClient client = JMSClientFactory.getJMSClient(CLIENT_ID, JMSClientFactory.JMS_VERSION_1, JMSClientFactory.PROVIDER_ACTIVEMQ);
		assertNotNull(client);
		assertTrue(client instanceof ActiveMQ11Client);
	}
	
	@Test(expected=RuntimeException.class)
	public void testGetJMSClientStringIntString_2() {
		JMSClientFactory.getJMSClient(CLIENT_ID, JMSClientFactory.JMS_VERSION_2, JMSClientFactory.PROVIDER_ACTIVEMQ);
	}
	
	@Test(expected=RuntimeException.class)
	public void testGetJMSClient_ImplNotExists() {
		JMSClientFactory.getJMSClient(CLIENT_ID, 3, "XXX");
	}

}
