package net.relaysoft.robot.jms.client;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.junit.EmbeddedActiveMQBroker;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import net.relaysoft.robot.jms.client.ActiveMQ11Client;

public class ActiveMQ11ClientTest {
	
	private static final String CLIENT_ID = "test";
	
	ActiveMQ11Client client;
	
	@Rule
	public EmbeddedActiveMQBroker broker = new EmbeddedActiveMQBroker();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		client = new ActiveMQ11Client(CLIENT_ID);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateConnectionFactory() throws Exception {
		ConnectionFactory factory = client.createConnectionFactory(createParameters());
		assertNotNull(factory);
		assertTrue(factory instanceof ActiveMQConnectionFactory);
		assertNotNull(factory.createConnection());
	}

	@Test
	public void testActiveMQ11Client() {
		assertNotNull(client);
		assertEquals(CLIENT_ID, client.getClientID());
	}
	
	private Map<String, Object> createParameters(){
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(ActiveMQ11Client.PARAM_BROKER_URL, broker.getVmURL());
		return parameters;
	}

}
