package net.relaysoft.robot.jms.client;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class IBMMQ11ClientTest {
	
	private static final String CLIENT_ID = "test";
	private static final String HOSTNAME = "localhost";
	private static final String QUEUE_MANAGER = "QM1";
	private static final String CHANNEL = "DEV.APP.SVRCONN";
	
	private static final int PORT = 11414;
	private static final int TRANSPORT_TYPE = 1;
	
	IBMMQ20Client client = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		client = new IBMMQ20Client(CLIENT_ID);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore("Can only be tested with IBM MQ") 
	public void testCreateConnectionFactory() throws Exception {
		ConnectionFactory factory = client.createConnectionFactory(createParameters());
		assertNotNull(factory);
		Connection conn = factory.createConnection();
		assertNotNull(conn);
	}
	
	private Map<String, Object> createParameters(){
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(IBMMQ20Client.PARAM_HOSTNAME, HOSTNAME);
		parameters.put(IBMMQ20Client.PARAM_PORT, PORT);
		parameters.put(IBMMQ20Client.PARAM_QUEUE_MANAGER, QUEUE_MANAGER);
		parameters.put(IBMMQ20Client.PARAM_CHANNEL, CHANNEL);
		parameters.put(IBMMQ20Client.PARAM_TRANSPORT_TYPE, TRANSPORT_TYPE);
		return parameters;
	}

}
